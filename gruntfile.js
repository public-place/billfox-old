'use strict';

/**
 * Module dependencies and build.
 */
var _ = require('lodash'),
    defaultAssets = require('./config/assets/development'),
    extraAssets = require('./config/assets/default'),
    fs = require('fs'),
    path = require('path');

module.exports = function (grunt) {
    // Project Configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        env: {
            dev: {
                NODE_ENV: 'development'
            },
            prod: {
                NODE_ENV: 'production'
            }
        },
        watch: {
            serverViews: {
                files: extraAssets.server.views,
                options: {
                    livereload: true
                }
            },
            serverJS: {
                files: _.union(extraAssets.server.gruntConfig, extraAssets.server.allJS),
                tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            clientViews: {
                files: defaultAssets.client.views,
                options: {
                    livereload: true
                }
            },
            clientJS: {
                files: _.union(defaultAssets.client.lib.js, defaultAssets.client.js),
                tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            clientCSS: {
                files: _.union(defaultAssets.client.lib.css, defaultAssets.client.css),
                tasks: ['csslint'],
                options: {
                    livereload: true
                }
            }
        },
        nodemon: {
            dev: {
                script: 'server.js',
                options: {
                    nodeArgs: ['--debug'],
                    ext: 'js,html',
                    watch: _.union(extraAssets.server.gruntConfig, extraAssets.server.views, extraAssets.server.allJS, extraAssets.server.config)
                }
            }
        },
        concurrent: {
            default: ['nodemon', 'watch'],
            debug: ['nodemon', 'watch', 'node-inspector'],
            options: {
                logConcurrentOutput: true
            }
        },
        jshint: {
            all: {
                src: _.union(extraAssets.server.gruntConfig, extraAssets.server.allJS, defaultAssets.client.js),
                options: {
                    jshintrc: true,
                    node: true,
                    mocha: true,
                    jasmine: true
                }
            }
        },
        csslint: {
            options: {
                csslintrc: '.csslintrc'
            },
            all: {
                src: defaultAssets.client.css
            }
        },
        ngAnnotate: {
            production: {
                files: {
                    'public/dist/application.js': defaultAssets.client.js,
                    'public/dist/vendor.js': defaultAssets.client.lib.js
                }
            }
        },
        uglify: {
            production: {
                options: {
                    mangle: false
                },
                files: {
                    'public/dist/application.min.js': 'public/dist/application.js',
                    'public/dist/vendor.min.js': 'public/dist/vendor.js'
                }
            }
        },
        cssmin: {
            combine: {
                files: {
                    'public/dist/application.min.css': defaultAssets.client.css,
                    'public/dist/vendor.min.css': defaultAssets.client.lib.css
                }
            }
        },
        'node-inspector': {
            custom: {
                options: {
                    'web-port': 1337,
                    'web-host': 'localhost',
                    'debug-port': 5858,
                    'save-live-edit': true,
                    'no-preload': true,
                    'stack-trace-limit': 50,
                    'hidden': []
                }
            }
        },
        protractor: {
            options: {
                configFile: 'protractor.conf.js',
                keepAlive: true,
                noColor: false
            },
            e2e: {
                options: {
                    args: {} // Target-specific arguments
                }
            }
        },
        copy: {
            localConfig: {
                src: 'config/env/local.example.js',
                dest: 'config/env/local.js',
                filter: function () {
                    return !fs.existsSync('config/env/local.js');
                }
            }
        }
    });

    grunt.event.on('coverage', function(lcovFileContents, done) {
        require('coveralls').handleInput(lcovFileContents, function(err) {
            if (err) {
                return done(err);
            }
            done();
        });
    });

    // Load NPM tasks
    require('load-grunt-tasks')(grunt);

    // Make sure upload directory exists
    grunt.task.registerTask('mkdir:upload', 'Task that makes sure upload directory exists.', function () {
        // Get the callback
        var done = this.async();

        grunt.file.mkdir(path.normalize(__dirname + '/modules/users/client/img/profile/uploads'));

        done();
    });

    // Connect to the MongoDB instance and load the models
    grunt.task.registerTask('mongoose', 'Task that connects to the MongoDB instance and loads the application models.', function () {
        // Get the callback
        var done = this.async();

        // Use mongoose configuration
        var mongoose = require('./config/lib/mongoose.js');

        // Connect to database
        mongoose.connect(function (db) {
            done();
        });
    });

    grunt.task.registerTask('server', 'Starting the server', function () {
        // Get the callback
        var done = this.async();

        var path = require('path');
        var app = require(path.resolve('./config/lib/app'));
        var server = app.start(function () {
            done();
        });
    });

    // Lint  and JavaScript files.
    grunt.registerTask('lint', ['sass', 'less', 'jshint', 'csslint']);

    // Lint project files and minify them into two production files.
    grunt.registerTask('build', ['env:dev', 'ngAnnotate', 'uglify', 'cssmin']);

    // Run the project in development mode
    grunt.registerTask('default', ['env:dev', 'mkdir:upload', 'copy:localConfig', 'concurrent:default']);

    // Run the project in debug mode
    grunt.registerTask('debug', ['env:dev', 'lint', 'mkdir:upload', 'copy:localConfig', 'concurrent:debug']);

    // Run the project in production mode
    grunt.registerTask('prod', ['build', 'env:prod', 'mkdir:upload', 'copy:localConfig', 'concurrent:default']);
};
