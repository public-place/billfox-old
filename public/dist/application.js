(function(window) {
    'use strict';

    var applicationModuleName = 'mean';

    var service = {
        applicationModuleName: applicationModuleName,
        applicationModuleVendorDependencies: ['ngResource', 'ngAnimate','ngSanitize','ngMessages', 'ui.router', 'angularFileUpload', 'slick', 'ui.select','ngMaterial', 'md-steppers', 'mdDataTable','datetime','datepicker','cl.paging'],
        registerModule: registerModule
    };

    window.ApplicationConfiguration = service;
	
	toastr.options = {
	
		"positionClass": "toast-top-center",  //"positionClass": "toast-top-full-width",
	}

    // Add a new vertical module
    function registerModule(moduleName, dependencies) {
        // Create angular module
        angular.module(moduleName, dependencies || []);

        // Add the module to the AngularJS configuration file
        angular.module(applicationModuleName).requires.push(moduleName);
    }
}(window));

(function (app) {
  'use strict';

  // Start by defining the main module and adding the module dependencies
  angular
    .module(app.applicationModuleName, app.applicationModuleVendorDependencies);

  // Setting HTML5 Location Mode
  angular
    .module(app.applicationModuleName)
    .config(bootstrapConfig)
  ;

  function bootstrapConfig($locationProvider, $httpProvider, $mdDateLocaleProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');

   $mdDateLocaleProvider.formatDate = function(date) {
        if(date) {
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            if(day < 10) day = '0'+day;
            var month = (monthIndex + 1);
            if((monthIndex + 1) < 10) month = '0'+(monthIndex + 1);
            return day + '.' + month + '.' + year;
        }
        return 'Enter Date';

  };

    $httpProvider.interceptors.push('authInterceptor');


  }

  bootstrapConfig.$inject = ['$locationProvider', '$httpProvider', '$mdDateLocaleProvider'];

  // Then define the init function for starting up the application
  angular.element(document).ready(init);

  function init() {
    // Fixing facebook bug with redirect
    if (window.location.hash && window.location.hash === '#_=_') {
      if (window.history && history.pushState) {
        window.history.pushState('', document.title, window.location.pathname);
      } else {
        // Prevent scrolling by storing the page's current scroll offset
        var scroll = {
          top: document.body.scrollTop,
          left: document.body.scrollLeft
        };
        window.location.hash = '';
        // Restore the scroll offset, should be flicker free
        document.body.scrollTop = scroll.top;
        document.body.scrollLeft = scroll.left;
      }
    }

    // Then init the app
    angular.bootstrap(document, [app.applicationModuleName]);
  }
}(ApplicationConfiguration));

(function (app) {
  'use strict';

  app.registerModule('core');
  app.registerModule('core.routes', ['ui.router']);
  app.registerModule('core.admin', ['core']);
  app.registerModule('core.admin.routes', ['ui.router']);
}(ApplicationConfiguration));

(function (app) {
  'use strict';

  app.registerModule('users');
  app.registerModule('users.admin');
  app.registerModule('users.admin.routes', ['ui.router', 'core.routes', 'users.admin.services']);
  app.registerModule('users.admin.services');
  app.registerModule('users.routes', ['ui.router', 'core.routes']);
  app.registerModule('users.services');
}(ApplicationConfiguration));

(function () {
  'use strict';

  angular
    .module('core')
    .run(routeFilter);

  routeFilter.$inject = ['$rootScope', '$state', 'Authentication'];

  function routeFilter($rootScope, $state, Authentication) {
    $rootScope.$on('$stateChangeStart', stateChangeStart);
    $rootScope.$on('$stateChangeSuccess', stateChangeSuccess);

    function stateChangeStart(event, toState, toParams, fromState, fromParams) {
      // Check authentication before changing state
      if (toState.data && toState.data.roles && toState.data.roles.length > 0) {
        var allowed = false;

        for (var i = 0, roles = toState.data.roles; i < roles.length; i++) {
          if ((roles[i] === 'guest') || (Authentication.user && Authentication.user.roles !== undefined && Authentication.user.roles.indexOf(roles[i]) !== -1)) {
            allowed = true;
            break;
          }
        }

        if (!allowed) {
          event.preventDefault();
          if (Authentication.user !== undefined && typeof Authentication.user === 'object') {
            $state.transitionTo('forbidden');
          } else {
            $state.go('authentication.signin').then(function () {
              // Record previous state
              storePreviousState(toState, toParams);
            });
          }
        }
      }
    }

    function stateChangeSuccess(event, toState, toParams, fromState, fromParams) {
      // Record previous state
      storePreviousState(fromState, fromParams);
    }

    // Store previous state
    function storePreviousState(state, params) {
      // only store this state if it shouldn't be ignored
      if (!state.data || !state.data.ignoreState) {
        $state.previous = {
          state: state,
          params: params,
          href: $state.href(state, params)
        };
      }
    }
  }
}());

(function() {
    'use strict';

    angular
        .module('core.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    function routeConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.rule(function($injector, $location) {
            var path = $location.path();
            var hasTrailingSlash = path.length > 1 && path[path.length - 1] === '/';

            if (hasTrailingSlash) {
                // if last character is a slash, return the same url without the slash
                var newPath = path.substr(0, path.length - 1);
                $location.replace().path(newPath);
            }
        });

        // Redirect to 404 when route not found
        $urlRouterProvider.otherwise(function($injector, $location) {
            $injector.get('$state').transitionTo('not-found', null, {
                location: false
            });
        });

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'modules/core/client/views/home.client.view.html',
                controller: 'HomeController',
                controllerAs: 'vm'
            })
            .state('records', {
                url: '/records',
                templateUrl: 'modules/core/client/views/records.client.view.html',
                controller: 'PatientController',
                controllerAs: 'vm'
            })
            .state('checklist', {
                url: '/checklist',
                templateUrl: 'modules/core/client/views/checklist.client.view.html',
                controller: 'PatientController',
                controllerAs: 'vm'
            })
            .state('print', {
                url: '/print/:patientId',
                templateUrl: 'modules/core/client/views/print.client.view.html',
                controller: 'PatientController',
                controllerAs: 'vm'
            })
            .state('view', {
                url: '/record/:patientId',
                templateUrl: 'modules/core/client/views/home.client.view.html',
                controller: 'HomeController',
                controllerAs: 'vm'
            })
        .state('not-found', {
                url: '/not-found',
                templateUrl: 'modules/core/client/views/404.client.view.html',
                data: {
                    ignoreState: true,
                    pageTitle: 'Not-Found'
                }
            })
            .state('bad-request', {
                url: '/bad-request',
                templateUrl: 'modules/core/client/views/400.client.view.html',
                data: {
                    ignoreState: true,
                    pageTitle: 'Bad-Request'
                }
            })
            .state('forbidden', {
                url: '/forbidden',
                templateUrl: 'modules/core/client/views/403.client.view.html',
                data: {
                    ignoreState: true,
                    pageTitle: 'Forbidden'
                }
            });
    }
}());

(function () {
  'use strict';

  angular
    .module('core')
    .controller('HeaderController', HeaderController);

  HeaderController.$inject = ['$scope', '$state', 'Authentication','$location', '$window'];

  function HeaderController($scope, $state, Authentication,$location, $window) {
    var vm = this;
    vm.authentication = Authentication;
    vm.isHome = isHome;
      vm.logoutMe = function(){
          $location.path('/api/auth/signout');
          $window.location.reload();
      }


    $scope.$on('$stateChangeSuccess', stateChangeSuccess);

      function isHome(){
          if($location.path() === '/'){
              return true;
          }else {
              return false;
          }
      }

    function stateChangeSuccess() {

      // Collapsing the menu after navigation
      vm.isCollapsed = false;
    }
  }
}());

(function() {
    'use strict';

    HomeController.$inject = ["$scope", "$state", "$http", "PatientsService", "$location", "$window", "$stateParams", "Authentication"];
    angular
        .module('core')
        .controller('HomeController', HomeController);

    function HomeController($scope, $state, $http, PatientsService, $location, $window, $stateParams, Authentication) {
        var vm = this;
        $scope.authentication = Authentication;
        if (!$scope.authentication.user) {
            $location.path('/admin/login');
        }

        vm.items = ['HWS-Syndrom', 'BWS-Syndrom', 'LWS-Syndrom', 'HWS-Dysfunktion', 'BWS-Dysfunktion', 'LWS-Dysfunktion', 'Rippen-Dysfunktion', 'Becken-Dysfunktion', 'Verdauungsinsuffizienz', 'Vorzugshaltung', 'Schädelasymmetrie', 'Migräne', 'craniomandibuläre Dysfunktion', 'Andere :'];
        vm.selected = [];
        vm.toggle = function(item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            } else {
                list.push(item);
            }
        };

        vm.diagnoseOther = function() {
            var index = vm.selected.indexOf('Andere :');
            if (vm.patient.diagnoseOther) {
                if (index === -1) {
                    vm.selected.push('Andere :');
                }
            } else {
                vm.selected.splice(index, 1);
            }
        }

        vm.exists = function(item, list) {
            return list.indexOf(item) > -1;
        };

        vm.isIndeterminate = function() {
            return (vm.selected.length !== 0 &&
                vm.selected.length !== vm.items.length);
        };

        vm.isChecked = function() {
            return vm.selected.length === vm.items.length;
        };

        vm.toggleAll = function() {
            if (vm.selected.length === vm.items.length) {
                vm.selected = [];
            } else if (vm.selected.length === 0 || vm.selected.length > 0) {
                vm.selected = vm.items.slice(0);
            }
        };

        vm.randomPrize = {};
        vm.reload = reload;
        vm.init = init;

        function init() {
            vm.patient = {};
            vm.patient = angular.copy(initialFetch(vm.patient));
            vm.selectedStep = 0;
            vm.stepProgress = 1;
            vm.maxStep = 6;
            vm.showBusyText = false;
            vm.stepData = [{
                    step: 1,
                    completed: false,
                    optional: false,
                    data: {}
                },
                {
                    step: 2,
                    completed: false,
                    optional: false,
                    data: {}
                },
                {
                    step: 3,
                    completed: false,
                    optional: false,
                    data: {}
                },
                {
                    step: 4,
                    completed: false,
                    optional: false,
                    data: {}
                },
                {
                    step: 5,
                    completed: false,
                    optional: false,
                    data: {}
                },
                {
                    step: 6,
                    completed: false,
                    optional: false,
                    data: {}
                }
            ];

            if ($stateParams.patientId) {
                vm.patient = {};
                findOne($stateParams.patientId);
            }


        }

        function findOne(id) {
            PatientsService.get({
                patientId: id
            }).$promise.then(function(data) {
                    console.log("====>>>", data)
                data = afterFetch(data);
                vm.patient = data
            });
        };

        function afterFetch(data) {
            var tempCopy = angular.copy(data);
            tempCopy.heute = moment(new Date(tempCopy.heute)).format('DD.MM.YYYY');
            for (var k = 1; k <= 10; k++) {
                if (tempCopy['behandlungsdatum_' + k]) {
                    tempCopy['behandlungsdatum_' + k] = moment(new Date(tempCopy['behandlungsdatum_' + k])).format('DD.MM.YYYY');
                }
            }
            if (tempCopy.gebDatumKind) tempCopy.gebDatumKind = moment(new Date(tempCopy.gebDatumKind)).format('DD.MM.YYYY');
            if (tempCopy.empfehlungsdatum) tempCopy.empfehlungsdatum = moment(new Date(tempCopy.empfehlungsdatum)).format('DD.MM.YYYY');

            if(tempCopy && tempCopy.diagnose){
				vm.selected = tempCopy.diagnose.split(',');
                for(var k=0; k < vm.selected.length; k++){
                    vm.selected[k] =    vm.selected[k].trim();
                }
			}
            return tempCopy;
        }

        function initialFetch(data) {
            var tempCopy = angular.copy(data);
            tempCopy.heute = moment(new Date()).format('DD.MM.YYYY');
            tempCopy.behandlungsdatum_1 = moment(new Date()).format('DD.MM.YYYY');
            return tempCopy;
        }

        vm.reset = function(n, value) {
            console.log(value);
            if (!value) {
                for (var k = n; k <= 10; k++) {
                    if (vm.patient['behandlungsdatum_' + k]) {
                        vm.patient['behandlungsdatum_' + k] = null;
                    }
                }
            }
        }

        init();

        vm.enableNextStep = function nextStep() {
            //do not exceed into max step

            if (vm.selectedStep >= vm.maxStep) {
                return;
            }
            //do not increment vm.stepProgress when submitting from previously completed step
            if (vm.selectedStep === vm.stepProgress - 1) {
                vm.stepProgress = vm.stepProgress + 1;
            }
            vm.selectedStep = vm.selectedStep + 1;
        }

        vm.moveToPreviousStep = function moveToPreviousStep() {
            if (vm.patient.grund == 'Empfehlung vom' && vm.selectedStep == 4) {
                vm.selectedStep = 3;
                vm.stepProgress = 4;
            }

            if (vm.patient.rechnungsart == 'Ausfallgebühr' && vm.selectedStep == 4) {
                vm.selectedStep = 0;
                vm.stepProgress = 1;
                return false;
            }

            if (vm.patient.grund == 'Diagnose' && vm.selectedStep == 3) {
                vm.selectedStep = 1;
                vm.stepProgress = 2;
                return false;
            }

            if (vm.selectedStep > 0) {
                vm.selectedStep = vm.selectedStep - 1;
            }
        }

        vm.submitCurrentStep = function submitCurrentStep(step, isSkip) {
            if (!vm.patient._id) {
                vm.showBusyText = true;
            }
            if (step == 'step1') {
                vm.stepData[0].data.completed = true;
                vm.showBusyText = false;

                if (vm.patient.rechnungsart == 'Ausfallgebühr') {
                    vm.selectedStep = 4;
                    vm.stepProgress = 5;
                } else {
                    vm.enableNextStep();
                }

                return false;
            }

            if (step == 'step2') {
                vm.stepData[1].data.completed = true;
                vm.showBusyText = false;

                if (vm.patient.grund == 'Empfehlung vom') {
                    vm.selectedStep = 2;
                    vm.stepProgress = 3;
                } else {
                    vm.selectedStep = 3;
                    vm.stepProgress = 4;
                }

                return false;
            }

            if (step == 'step3') {
                vm.stepData[2].data.completed = true;
                vm.showBusyText = false;
                if (vm.patient.grund == 'Empfehlung vom') {
                    vm.selectedStep = 4;
                    vm.stepProgress = 5;
                }
                //vm.enableNextStep();
                return false;
            }

            if (step == 'step4') {
                vm.stepData[3].data.completed = true;
                vm.showBusyText = false;
                vm.enableNextStep();
                return false;
            }

            if (step == 'step5') {
                vm.showBusyText = false;
                $scope.isLoading = true;
                vm.stepData[3].data.completed = true;
                var resultData = formatBeforeSave();

                if (resultData._id) {
                    //var patient = new PatientsService (resultData);
                    resultData.$update(function(response) {
                        toastr.success('Document successfully update');
                        $scope.isLoading = false;
                        vm.enableNextStep();
                        vm.stepData[4].data.completed = true;
                    }, function(errorResponse) {
                        toastr.error('Error : ' + errorResponse.data.message);
                    });
                } else {
                    var patient = new PatientsService(resultData);
                    patient.$save(function(response) {
                        toastr.success('Document  successfully added');
                        vm.enableNextStep();
                        vm.stepData[4].data.completed = true;
                    }, function(errorResponse) {
                        toastr.error('Error : ' + errorResponse.data.message);
                    });
                }
            }
        }

        function formatBeforeSave() {
            var tempCopy = angular.copy(vm.patient);
            tempCopy.heute = datefor(tempCopy.heute);

            if (tempCopy.gebDatumKind) {
                tempCopy.gebDatumKind = datefor(tempCopy.gebDatumKind);
            }

            if (tempCopy.empfehlungsdatum) {
                tempCopy.empfehlungsdatum = datefor(tempCopy.empfehlungsdatum);
            }

            for (var k = 1; k <= 10; k++) {
                if (tempCopy['behandlungsdatum_' + k]) {
                    tempCopy['behandlungsdatum_' + k] = datefor(tempCopy['behandlungsdatum_' + k]);
                }
            }

            //diagnose
            var str = '';
            if (vm.selected.length && vm.selected.length > 1) {
                for (var k = 0; k < vm.selected.length; k++) {
                    /*if(vm.selected[k].indexOf('Andere :') > -1){
                        str += 'Andere : '+vm.patient.diagnoseOther || '' +',';
                    }else {

                    }*/
                    str += vm.selected[k] + ', ';
                }
                str = str.trim();
                console.log(tempCopy.diagnose);
                tempCopy.diagnose = str.slice(0, -1);
                console.log(tempCopy.diagnose);
            } else {
                tempCopy.diagnose = vm.selected[0];
            }


            return tempCopy;
        }

        function datefor(d) {
            return new Date(moment(d, 'DD.MM.YYYY').format('MM-DD-YYYY')).getTime();
            //return new Date(d).getTime();
        }

        var query = $location.search();

        function reload() {
            $location.$$search = {};
            $location.path('/');
            $window.location.reload();
        }

        vm.getCity = function(pincode) {
            $http.get('/api/getCity/' + pincode).success(function(data) {
                vm.patient.stadt = data.city ? data.city.city : '';

            }).error(function() {

            });
        }

    }
}());

(function () {
  'use strict';

  angular.module('core')
    .directive('pageTitle', pageTitle);

  pageTitle.$inject = ['$rootScope', '$timeout', '$interpolate', '$state'];

  function pageTitle($rootScope, $timeout, $interpolate, $state) {
    var directive = {
      restrict: 'A',
      link: link
    };

    return directive;

    function link(scope, element) {
      $rootScope.$on('$stateChangeSuccess', listener);

      function listener(event, toState) {
        var title = (getTitle($state.$current));
        $timeout(function () {
          element.text(title);
        }, 0, false);
      }

      function getTitle(currentState) {
        var applicationCoreTitle = 'Abrechnung Praxis';
        var workingState = currentState;
        if (currentState.data) {
          workingState = (typeof workingState.locals !== 'undefined') ? workingState.locals.globals : workingState;
          var stateTitle = $interpolate(currentState.data.pageTitle)(workingState);
          return applicationCoreTitle + ' - ' + stateTitle;
        } else {
          return applicationCoreTitle;
        }
      }
    }
  }
}());

(function () {
  'use strict';

  // https://gist.github.com/rhutchison/c8c14946e88a1c8f9216

  angular
    .module('core')
    .directive('showErrors', showErrors);

  showErrors.$inject = ['$timeout', '$interpolate'];

  function showErrors($timeout, $interpolate) {
    var directive = {
      restrict: 'A',
      require: '^form',
      compile: compile
    };

    return directive;

    function compile(elem, attrs) {
      if (attrs.showErrors.indexOf('skipFormGroupCheck') === -1) {
        if (!(elem.hasClass('form-group') || elem.hasClass('input-group'))) {
          throw new Error('show-errors element does not have the \'form-group\' or \'input-group\' class');
        }
      }

      return linkFn;

      function linkFn(scope, el, attrs, formCtrl) {
        var inputEl,
          inputName,
          inputNgEl,
          options,
          showSuccess,
          initCheck = false,
          showValidationMessages = false;

        options = scope.$eval(attrs.showErrors) || {};
        showSuccess = options.showSuccess || false;
        inputEl = el[0].querySelector('.form-control[name]') || el[0].querySelector('[name]');
        inputNgEl = angular.element(inputEl);
        inputName = $interpolate(inputNgEl.attr('name') || '')(scope);

        if (!inputName) {
          throw new Error('show-errors element has no child input elements with a \'name\' attribute class');
        }

        scope.$watch(function () {
          return formCtrl[inputName] && formCtrl[inputName].$invalid;
        }, toggleClasses);

        scope.$on('show-errors-check-validity', checkValidity);
        scope.$on('show-errors-reset', reset);

        function checkValidity(event, name) {
          if (angular.isUndefined(name) || formCtrl.$name === name) {
            initCheck = true;
            showValidationMessages = true;

            return toggleClasses(formCtrl[inputName].$invalid);
          }
        }

        function reset(event, name) {
          if (angular.isUndefined(name) || formCtrl.$name === name) {
            return $timeout(function () {
              el.removeClass('has-error');
              el.removeClass('has-success');
              showValidationMessages = false;
            }, 0, false);
          }
        }

        function toggleClasses(invalid) {
          el.toggleClass('has-error', showValidationMessages && invalid);

          if (showSuccess) {
            return el.toggleClass('has-success', showValidationMessages && !invalid);
          }
        }
      }
    }
  }
}());

(function () {
  'use strict';

  angular
    .module('core')
    .factory('authInterceptor', authInterceptor);

  authInterceptor.$inject = ['$q', '$injector', 'Authentication'];

  function authInterceptor($q, $injector, Authentication) {
    var service = {
      responseError: responseError
    };

    return service;

    function responseError(rejection) {
      if (!rejection.config.ignoreAuthModule) {
        switch (rejection.status) {
          case 401:
            // Deauthenticate the global user
            Authentication.user = null;
            $injector.get('$state').transitionTo('authentication.signin');
            break;
          case 403:
            $injector.get('$state').transitionTo('forbidden');
            break;
        }
      }
      // otherwise, default behaviour
      return $q.reject(rejection);
    }
  }
}());

(function () {
  'use strict';

  // Setting up route
  angular
    .module('users.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.users', {
        url: '/users',
        templateUrl: 'modules/users/client/views/admin/list-users.client.view.html',
        controller: 'UserListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Users List'
        }
      })
      .state('admin.user', {
        url: '/users/:userId',
        templateUrl: 'modules/users/client/views/admin/view-user.client.view.html',
        controller: 'UserController',
        controllerAs: 'vm',
        resolve: {
          userResolve: getUser
        },
        data: {
          pageTitle: 'Edit {{ userResolve.displayName }}'
        }
      })
      .state('admin.user-edit', {
        url: '/users/:userId/edit',
        templateUrl: 'modules/users/client/views/admin/edit-user.client.view.html',
        controller: 'UserController',
        controllerAs: 'vm',
        resolve: {
          userResolve: getUser
        },
        data: {
          pageTitle: 'Edit User {{ userResolve.displayName }}'
        }
      });

    getUser.$inject = ['$stateParams', 'AdminService'];

    function getUser($stateParams, AdminService) {
      return AdminService.get({
        userId: $stateParams.userId
      }).$promise;
    }
  }
}());

(function() {
    'use strict';

    // Setting up route
    angular
        .module('users.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {
        // Users state routing
        $stateProvider
            .state('settings', {
                abstract: true,
                url: '/settings',
                templateUrl: 'modules/users/client/views/settings/settings.client.view.html',
                controller: 'SettingsController',
                controllerAs: 'vm',
                data: {
                    roles: ['user', 'admin']
                }
            })
            .state('settings.profile', {
                url: '/profile',
                templateUrl: 'modules/users/client/views/settings/edit-profile.client.view.html',
                controller: 'EditProfileController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Settings'
                }
            })


        .state('settings.password', {
                url: '/password',
                templateUrl: 'modules/users/client/views/settings/change-password.client.view.html',
                controller: 'ChangePasswordController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Settings password'
                }
            })
            .state('login', {
                url: '/admin/login',
                templateUrl: 'modules/users/client/views/authentication/signin.client.view.html',
                controller: 'AuthenticationController',
                controllerAs: 'vm'
            })
            .state('password', {
                abstract: true,
                url: '/password',
                template: '<ui-view/>'
            })
            .state('password.forgot', {
                url: '/forgot',
                templateUrl: 'modules/users/client/views/password/forgot-password.client.view.html',
                controller: 'PasswordController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Password forgot'
                }
            })
            .state('password.reset', {
                abstract: true,
                url: '/reset',
                template: '<ui-view/>'
            })
            .state('password.reset.invalid', {
                url: '/invalid',
                templateUrl: 'modules/users/client/views/password/reset-password-invalid.client.view.html',
                data: {
                    pageTitle: 'Password reset invalid'
                }
            })
            .state('password.reset.success', {
                url: '/success',
                templateUrl: 'modules/users/client/views/password/reset-password-success.client.view.html',
                data: {
                    pageTitle: 'Password reset success'
                }
            })
            .state('password.reset.form', {
                url: '/:token',
                templateUrl: 'modules/users/client/views/password/reset-password.client.view.html',
                controller: 'PasswordController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Password reset form'
                }
            });
    }
}());

(function () {
  'use strict';

  angular
    .module('users.admin')
    .controller('UserListController', UserListController);

  UserListController.$inject = ['$scope', '$filter', 'AdminService'];

  function UserListController($scope, $filter, AdminService) {
    var vm = this;
    vm.buildPager = buildPager;
    vm.figureOutItemsToDisplay = figureOutItemsToDisplay;
    vm.pageChanged = pageChanged;

    AdminService.query(function (data) {
      vm.users = data;
      vm.buildPager();
    });

    function buildPager() {
      vm.pagedItems = [];
      vm.itemsPerPage = 15;
      vm.currentPage = 1;
      vm.figureOutItemsToDisplay();
    }

    function figureOutItemsToDisplay() {
      vm.filteredItems = $filter('filter')(vm.users, {
        $: vm.search
      });
      vm.filterLength = vm.filteredItems.length;
      var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
      var end = begin + vm.itemsPerPage;
      vm.pagedItems = vm.filteredItems.slice(begin, end);
    }

    function pageChanged() {
      vm.figureOutItemsToDisplay();
    }
  }
}());

(function () {
  'use strict';

  angular
    .module('users.admin')
    .controller('UserController', UserController);

  UserController.$inject = ['$scope', '$state', '$window', 'Authentication', 'userResolve'];

  function UserController($scope, $state, $window, Authentication, user) {
    var vm = this;

    vm.authentication = Authentication;
    vm.user = user;
    vm.remove = remove;
    vm.update = update;

    function remove(user) {
      if ($window.confirm('Are you sure you want to delete this user?')) {
        if (user) {
          user.$remove();

          vm.users.splice(vm.users.indexOf(user), 1);
        } else {
          vm.user.$remove(function () {
            $state.go('admin.users');
          });
        }
      }
    }

    function update(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.userForm');

        return false;
      }

      var user = vm.user;

      user.$update(function () {
        $state.go('admin.user', {
          userId: user._id
        });
      }, function (errorResponse) {
        vm.error = errorResponse.data.message;
      });
    }
  }
}());

(function () {
  'use strict';

  angular
    .module('users')
    .controller('AuthenticationController', AuthenticationController);

  AuthenticationController.$inject = ['$scope', '$state', '$http', '$location', '$window', 'Authentication'];

  function AuthenticationController($scope, $state, $http, $location, $window, Authentication) {
    var vm = this;

    $scope.authentication = Authentication;
      $scope.signCredentials = {};
      $scope.signin = signin;
    vm.callOauthProvider = callOauthProvider;

    // Get an eventual error defined in the URL query string:
    vm.error = $location.search().err;

    // If user is signed in then redirect back home
    if ($scope.authentication.user) {
      $location.path('/');
    }

    function signin(isValid) {
      vm.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.userForm');
      }

      $http.post('/api/auth/signin', $scope.signCredentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;
          toastr.info('Login successfully');
        // And redirect to the previous or home page
          $location.path('/');
      }).error(function (response) {
          toastr.error('Error : '+response.message);
      });
    }

    // OAuth provider request
    function callOauthProvider(url) {
      if ($state.previous && $state.previous.href) {
        url += '?redirect_to=' + encodeURIComponent($state.previous.href);
      }

      // Effectively call OAuth authentication route:
      $window.location.href = url;
    }
  }
}());

(function () {
  'use strict';

  angular
    .module('users')
    .controller('PasswordController', PasswordController);

  PasswordController.$inject = ['$scope', '$stateParams', '$http', '$location', 'Authentication', 'PasswordValidator'];

  function PasswordController($scope, $stateParams, $http, $location, Authentication, PasswordValidator) {
    var vm = this;

    vm.resetUserPassword = resetUserPassword;
    vm.askForPasswordReset = askForPasswordReset;
    vm.authentication = Authentication;
    vm.getPopoverMsg = PasswordValidator.getPopoverMsg;

    // If user is signed in then redirect back home
    if (vm.authentication.user) {
      $location.path('/');
    }

    // Submit forgotten password account id
    function askForPasswordReset(isValid) {
      vm.success = vm.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.forgotPasswordForm');

        return false;
      }

      $http.post('/api/auth/forgot', vm.credentials).success(function (response) {
        // Show user success message and clear form
        vm.credentials = null;
        vm.success = response.message;

      }).error(function (response) {
        // Show user error message and clear form
        vm.credentials = null;
        vm.error = response.message;
      });
    }

    // Change user password
    function resetUserPassword(isValid) {
      vm.success = vm.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.resetPasswordForm');

        return false;
      }

      $http.post('/api/auth/reset/' + $stateParams.token, vm.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        vm.passwordDetails = null;

        // Attach user profile
        Authentication.user = response;

        // And redirect to the index page
        $location.path('/password/reset/success');
      }).error(function (response) {
        vm.error = response.message;
      });
    }
  }
}());

(function () {
  'use strict';

  angular
    .module('users')
    .controller('PatientController', PatientController);

    PatientController.$inject = ['$scope', '$stateParams', '$http', '$location', 'Authentication', 'FileUploader', '$window', '$timeout', 'PatientsService', '$mdDialog'];

  function PatientController($scope, $stateParams, $http, $location, Authentication, FileUploader, $window, $timeout, PatientsService, $mdDialog) {
    var vm = this;
      vm.patients = [];
      vm.patient  = {};
      vm.list = [1,2,3,4,5,6,7,8,9,10]
	  vm.find = find;
      vm.authentication = Authentication;
      if (!vm.authentication.user) {
          $location.path('/admin/login');
      }

      $scope.dialog = { 'result' : 'menk.osteo@gmail.com'};


      vm.showPrompt = function(ev, patient) {
          // Appending dialog to document.body to cover sidenav in docs app
          var confirm = $mdDialog.prompt()
              .title('Enter email address')
              .placeholder('menk.osteo@gmail.com')
              //.initialValue('menk.osteo@gmail.com')
              .ariaLabel('Email')
              .targetEvent(ev)
              .ok('Okay!')
              .cancel('Cancel');

          $mdDialog.show(confirm).then(function(result) {
              var patients = [];

              for(var k=0; k < vm.patients.length; k++){
                    if(vm.patients[k].isSelect) patients.push(vm.patients[k]._id);
              }
              console.log("========>>>", patients);

              if(!result)   result = 'menk.osteo@gmail.com';
              $http.post('/api/sendEmail', {email : result, patients : patients}).success(function(data){
                  console.log(data);
                  vm.scratchers = data.result;
                  vm.resultHeader = data.scratcherCount;
              }).error(function(){

              });

          }, function() {
              $scope.status = 'You didn\'t name your dog.';
          });
      };

      $scope.setToAllFn = function(status){
         for(var k=0; k < vm.patients.length; k++){
             vm.patients[k].isSelect = status;
         }
      }

      if (vm.authentication.user && vm.authentication.user.roles && vm.authentication.user.roles[0] !== 'admin') {
          $location.path('/');
      }

      vm.isOpen = false;
        if( $stateParams.patientId){
            findOne($stateParams.patientId);
        }

      function findOne(id) {
          vm.patient = PatientsService.get({
              patientId: id
          });
      };

      vm.formatPrintf = function(str, andre) {
            if(str) {
                str = str.replace('Andere :', '');
                str += andre;
            }
            return str;
      }

      vm.format = function(str) {
          if(!str)    return str = '';
          str = Number(str);
          str = str.toFixed(2);
          str = str.toString();
          if(str.indexOf('.') > -1){
              str.replace('.',',')
          }else {
              str = str+',00';
          }

          str = str+' €';
          return str;
      }

      $scope.paging = {
          page: 1,
          onPageChanged: find
      };

      function find(q) {
          $http.get('/api/patients', {
              params: { page: $scope.paging.page, q : q }
          }).success(function(data){
              vm.patients = data.result;
              if(data.count)    $scope.paging.total = Math.ceil(data.count / 10);
              if(data.total && data.total[0] && data.total[0].total) vm.getTotal = data.total[0].total;
          }).error(function(){

          });
      };


      vm.update = function(ev, result, isCheckList) {
          result = new PatientsService(result);
          if(!result.isPaid && isCheckList){
              var confirm = $mdDialog.confirm()
                  .title('do u wirklich deaktivieren wollen?')
                  .targetEvent(ev)
                  .ok('Ok')
                  .cancel('Abbrechen');

              $mdDialog.show(confirm).then(function() {
                  result.$update(function(response) {
                      toastr.success('Document successfully update');
                      find();
                  }, function(errorResponse) {
                      toastr.error('Error : '+errorResponse.data.message);
                  });
              }, function() {
                  result.isPaid = true;
              });

          }else {
              result.$update(function(response) {
                  toastr.success('Document successfully update');
                  find();
              }, function(errorResponse) {
                  toastr.error('Error : '+errorResponse.data.message);
              });
          }
      }

      if(!$stateParams.patientId) {
          find();
      }

      vm.showConfirm = function(ev, patient,index) {
          // Appending dialog to document.body to cover sidenav in docs app
          var confirm = $mdDialog.confirm()
              .title('Möchtest du den Eintrag löschen?')
              .textContent("Was weck ist, ist wech.")
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('Ok')
              .cancel('Abbrechen');

          $mdDialog.show(confirm).then(function() {
			  patient = new PatientsService(patient);	
              patient.$remove();
              vm.patients.splice(index, 1);
              toastr.success('Document successfully Deleted');
          }, function() {
              $scope.status = 'You decided to keep your debt.';
          });
      };

  }
}());

(function () {
  'use strict';

  angular
    .module('users')
    .controller('ScratcherController', ScratcherController);

    ScratcherController.$inject = ['$scope', '$stateParams', '$http', '$location', 'Authentication', 'FileUploader', '$window', '$timeout', 'ScratchersService'];

  function ScratcherController($scope, $stateParams, $http, $location, Authentication, FileUploader, $window, $timeout, ScratchersService ) {
    var vm = this;
    $scope.scratchers = [];
	  vm.init = init;
      vm.create = create;
      vm.remove = remove;
      vm.update = update;
      vm.getOne = getOne;
	  vm.find = find;
	  vm.isOpen = false;

      vm.authentication = Authentication;
      if (!vm.authentication.user) {
          $location.path('/');
      }

      function getDay(index) {
          var weekday= ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday","Friday","Saturday"];
          return weekday[index];
      }
		
		function init() {
			vm.scratcher = {};
            var stateDate = new Date();
            vm.scratcher.announceDay = getDay(stateDate.getDay());
            stateDate.setHours(0);
            stateDate.setMinutes(30);
            vm.scratcher.endTime = endDate;
            var endDate = new Date();
            endDate.setHours(23);
            endDate.setMinutes(55);

			vm.scratcher.startTime = stateDate;
			vm.scratcher.endTime = endDate;
			
		}
		init();
		find();
      function getOne(scratcher) {
          vm.scratcher = scratcher;
      }


      function beforeAddAndUpdate() {
          var temp =  angular.copy(vm.scratcher);
          temp.tempStartHour = new Date(temp.startTime).getHours();
          temp.tempStartMinutes = new Date(temp.startTime).getMinutes();

          temp.tempEndHour = new Date(temp.endTime).getHours();
          temp.tempEndMinutes = new Date(temp.endTime).getMinutes();

          temp.startTime = new Date(temp.startTime).getTime();
          temp.endTime = new Date(temp.endTime).getTime();

          return temp;
      }

      // Create new Scratcher
      function create() {
          // Create new Scratcher object
          var temp =  beforeAddAndUpdate();
          var scratcher = new ScratchersService (temp);

          // Redirect after save
          scratcher.$save(function(response) {
              //vm.scratchers.push(response);
              toastr.success('Scratcher successfully added');
              init();
              find();

          }, function(errorResponse) {
              toastr.error('Error : '+errorResponse.data.message);
          });
      };

      // Remove existing Scratcher
      function remove(scratcher){
          if ( scratcher ) {
              scratcher.$remove();

              for (var i in vm.scratchers) {
                  if (vm.scratchers [i] === scratcher) {
                      vm.scratchers.splice(i, 1);
                  }
              }
          }
      };

      // Update existing Scratcher
      function update() {
		var temp =  beforeAddAndUpdate();
          var scratcher = new ScratchersService (temp);
          scratcher.$update(function() {
               toastr.success('Scratcher successfully updated');
          }, function(errorResponse) {
              toastr.error('Error : '+errorResponse.data.message);
          });
      };



      // Find a list of Scratchers
      function find() {
         $http.get('/api/scratchers').success(function(data){
             console.log(data);
             vm.scratchers = data.result;
             vm.resultHeader = data.scratcherCount;
         }).error(function(){

         });
      };

      

      // Find existing Scratcher
      $scope.findOne = function(id) {
          $scope.scratcher = ScratchersService.get({
              scratcherId: id
          });
      };

  }
}());

(function () {
  'use strict';

  angular
    .module('users')
    .controller('ChangePasswordController', ChangePasswordController);

  ChangePasswordController.$inject = ['$scope', '$http', 'Authentication', 'PasswordValidator'];

  function ChangePasswordController($scope, $http, Authentication, PasswordValidator) {
    var vm = this;

    vm.user = Authentication.user;
    vm.changeUserPassword = changeUserPassword;
    vm.getPopoverMsg = PasswordValidator.getPopoverMsg;

    // Change user password
    function changeUserPassword(isValid) {
      vm.success = vm.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.passwordForm');

        return false;
      }

      $http.post('/api/users/password', vm.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.$broadcast('show-errors-reset', 'vm.passwordForm');
        vm.success = true;
        vm.passwordDetails = null;
      }).error(function (response) {
        vm.error = response.message;
      });
    }
  }
}());

(function () {
  'use strict';

  angular
    .module('users')
    .controller('ChangeProfilePictureController', ChangeProfilePictureController);

  ChangeProfilePictureController.$inject = ['$scope', '$timeout', '$window', 'Authentication', 'FileUploader'];

  function ChangeProfilePictureController($scope, $timeout, $window, Authentication, FileUploader) {
    var vm = this;

    vm.user = Authentication.user;
    vm.imageURL = vm.user.profileImageURL;
    vm.uploadProfilePicture = uploadProfilePicture;

    vm.cancelUpload = cancelUpload;
    // Create file uploader instance
    vm.uploader = new FileUploader({
      url: 'api/users/picture',
      alias: 'newProfilePicture',
      onAfterAddingFile: onAfterAddingFile,
      onSuccessItem: onSuccessItem,
      onErrorItem: onErrorItem
    });

    // Set file uploader image filter
    vm.uploader.filters.push({
      name: 'imageFilter',
      fn: function (item, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
    });

    // Called after the user selected a new picture file
    function onAfterAddingFile(fileItem) {
      if ($window.FileReader) {
        var fileReader = new FileReader();
        fileReader.readAsDataURL(fileItem._file);

        fileReader.onload = function (fileReaderEvent) {
          $timeout(function () {
            vm.imageURL = fileReaderEvent.target.result;
          }, 0);
        };
      }
    }

    // Called after the user has successfully uploaded a new picture
    function onSuccessItem(fileItem, response, status, headers) {
      // Show success message
      vm.success = true;

      // Populate user object
      vm.user = Authentication.user = response;

      // Clear upload buttons
      cancelUpload();
    }

    // Called after the user has failed to uploaded a new picture
    function onErrorItem(fileItem, response, status, headers) {
      // Clear upload buttons
      cancelUpload();

      // Show error message
      vm.error = response.message;
    }

    // Change user profile picture
    function uploadProfilePicture() {
      // Clear messages
      vm.success = vm.error = null;

      // Start upload
      vm.uploader.uploadAll();
    }

    // Cancel the upload process
    function cancelUpload() {
      vm.uploader.clearQueue();
      vm.imageURL = vm.user.profileImageURL;
    }
  }
}());

(function () {
  'use strict';

  angular
    .module('users')
    .controller('DoelenController', GoalController);

    GoalController.$inject = ['$scope', '$stateParams', '$http', '$location', 'Authentication', 'FileUploader', '$window', '$timeout'];

  function GoalController($scope, $stateParams, $http, $location, Authentication, FileUploader, $window, $timeout ) {
    var vm = this;
    vm.dolens = [];
    vm.dolensCount = [];
    $scope.authentication = Authentication;


      function getDoelen() {
              $http.get('/api/goals/doelen').success(function (response) {
                  vm.dolens = response.result;
                  vm.dolensCount = response.goalCount;
              }).error(function (response) {
                  // Show user error message and clear form
                  vm.credentials = null;
                  vm.error = response.message;
              });
      }

      getDoelen();

  }
}());

(function () {
  'use strict';

  angular
    .module('users')
    .controller('EditProfileController', EditProfileController);

  EditProfileController.$inject = ['$scope', '$http', '$location', 'UsersService', 'Authentication', 'FileUploader', '$window', '$timeout'];

  function EditProfileController($scope, $http, $location, UsersService, Authentication, FileUploader, $window, $timeout ) {
    var vm = this;
    vm.user = Authentication.user;
    vm.goal = {};
    vm.imageType = { type : 'image'};
    vm.updateGoalInfo = updateGoalInfo;
    vm.updateUserProfile = updateUserProfile;

      function clear() {
          vm.actionSelection = {};
          vm.actionSelection.isProfileEdit = false;
          vm.actionSelection.isGoalEdit = false;
      }
      clear();

    // Update a user profile
    function updateUserProfile(isValid, formType) {
        console.log(isValid);
      vm.success = vm.error = null;
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.userForm');
        return false;
      }

      var user = new UsersService(vm.user);

      user.$update(function (response) {
        toastr.success(formType +' successfully updated');
        $scope.$broadcast('show-errors-reset', 'vm.userForm');
        clear();
        Authentication.user = response;
      }, function (response) {
        vm.error = response.data.message;
      });



    }


      vm.imageURL = vm.user.profileImageURL;
      vm.uploadProfilePicture = uploadProfilePicture;

      vm.cancelUpload = cancelUpload;
      // Create file uploader instance
      vm.uploader = new FileUploader({
          alias: 'newProfilePicture',
          onAfterAddingFile: onAfterAddingFile,
          onBeforeUploadItem : onBeforeUploadItem,
          onSuccessItem: onSuccessItem,
          onErrorItem: onErrorItem
      });

      //-----------------------------------

      /* Get User Goal Info */

      getUserGoalInfo();
      function getUserGoalInfo(){
          $http.get('/api/goals/'+vm.user.goal).success(function (response) {
              console.log(response);
              vm.goal = response;
              vm.goalImageUrl = vm.goal.imageURL;
          }).error(function (response) {
              // Show user error message and clear form
              vm.credentials = null;
              vm.error = response.message;
          });
      }

      function updateGoalInfo() {
          console.log(vm.goal);
          $http.put('/api/goals/'+vm.user.goal, vm.goal).success(function (response) {
              vm.goal = response;
              vm.goalImageUrl = vm.goal.imageURL;
              vm.actionSelection.isGoalEdit = false;
              toastr.success('Goal info successfully updated');
          }).error(function (response) {
              // Show user error message and clear form
              vm.credentials = null;
              vm.error = response.message;
          });
      }

      $scope.availableCategories = ['Dieren', 'Gezondheid', 'Kinderen', 'Mensen', 'Natuur', 'Religieus'];







      // Set file uploader image filter
      vm.uploader.filters.push({
          name: 'imageFilter',
          fn: function (item, options) {
              var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
              return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
          }
      });

      // Called after the user selected a new picture file
      function onAfterAddingFile(fileItem) {
          if ($window.FileReader) {
              var fileReader = new FileReader();
              fileReader.readAsDataURL(fileItem._file);

              fileReader.onload = function (fileReaderEvent) {
                  $timeout(function () {
                      if(vm.imageType.type == 'image') {
                          vm.imageURL = fileReaderEvent.target.result;
                      }

                      if(vm.imageType.type == 'goal') {
                          vm.goalImageUrl  = fileReaderEvent.target.result;
                      }

                  }, 0);
              };
          }
      }

      function onBeforeUploadItem(item) {
          item.url = 'api/users/picture?imageType='+vm.imageType.type;
      }

          // Called after the user has successfully uploaded a new picture
      function onSuccessItem(fileItem, response, status, headers) {
          // Show success message
          toastr.success('image successfully updated');
          if(vm.imageType.type == 'image'){
              vm.user = Authentication.user = response;
          }else {
              vm.goal.imageURL =  response.imageUrl;
              vm.goalImageUrl = response.imageUrl;
          }

          // Populate user object


          // Clear upload buttons
          cancelUpload();
      }

      // Called after the user has failed to uploaded a new picture
      function onErrorItem(fileItem, response, status, headers) {
          // Clear upload buttons
          cancelUpload();

          // Show error message
          vm.error = response.message;
      }

      // Change user profile picture
      function uploadProfilePicture() {
          // Clear messages
          vm.success = vm.error = null;
          vm.uploader.queue[0].formData = vm.imageType.type;
          // Start upload
          vm.uploader.uploadAll();
      }

      // Cancel the upload process
      function cancelUpload() {
          vm.uploader.clearQueue();
          vm.imageURL = vm.user.profileImageURL;
          vm.goalImageUrl = vm.goal.imageURL;
      }
  }
}());

(function () {
  'use strict';

  angular
    .module('users')
    .controller('SocialAccountsController', SocialAccountsController);

  SocialAccountsController.$inject = ['$scope', '$http', 'Authentication'];

  function SocialAccountsController($scope, $http, Authentication) {
    var vm = this;

    vm.user = Authentication.user;
    vm.hasConnectedAdditionalSocialAccounts = hasConnectedAdditionalSocialAccounts;
    vm.isConnectedSocialAccount = isConnectedSocialAccount;
    vm.removeUserSocialAccount = removeUserSocialAccount;

    // Check if there are additional accounts
    function hasConnectedAdditionalSocialAccounts() {
      return ($scope.user.additionalProvidersData && Object.keys($scope.user.additionalProvidersData).length);
    }

    // Check if provider is already in use with current user
    function isConnectedSocialAccount(provider) {
      return vm.user.provider === provider || (vm.user.additionalProvidersData && vm.user.additionalProvidersData[provider]);
    }

    // Remove a user social account
    function removeUserSocialAccount(provider) {
      vm.success = vm.error = null;

      $http.delete('/api/users/accounts', {
        params: {
          provider: provider
        }
      }).success(function (response) {
        // If successful show success message and clear form
        vm.success = true;
        vm.user = Authentication.user = response;
      }).error(function (response) {
        vm.error = response.message;
      });
    }
  }
}());

(function () {
  'use strict';

  angular
    .module('users')
    .controller('SettingsController', SettingsController);

  SettingsController.$inject = ['$scope', 'Authentication'];

  function SettingsController($scope, Authentication) {
    var vm = this;

    vm.user = Authentication.user;
  }
}());

(function () {
  'use strict';

  angular
    .module('users')
    .directive('passwordValidator', passwordValidator);

  passwordValidator.$inject = ['PasswordValidator'];

  function passwordValidator(PasswordValidator) {
    var directive = {
      require: 'ngModel',
      link: link
    };

    return directive;

    function link(scope, element, attrs, ngModel) {
      ngModel.$validators.requirements = function (password) {
        var status = true;
        if (password) {
          var result = PasswordValidator.getResult(password);
          var requirementsIdx = 0;

          // Requirements Meter - visual indicator for users
          var requirementsMeter = [{
            color: 'danger',
            progress: '20'
          }, {
            color: 'warning',
            progress: '40'
          }, {
            color: 'info',
            progress: '60'
          }, {
            color: 'primary',
            progress: '80'
          }, {
            color: 'success',
            progress: '100'
          }];

          if (result.errors.length < requirementsMeter.length) {
            requirementsIdx = requirementsMeter.length - result.errors.length - 1;
          }

          scope.requirementsColor = requirementsMeter[requirementsIdx].color;
          scope.requirementsProgress = requirementsMeter[requirementsIdx].progress;

          if (result.errors.length) {
            scope.getPopoverMsg = PasswordValidator.getPopoverMsg();
            scope.passwordErrors = result.errors;
            status = false;
          } else {
            scope.getPopoverMsg = '';
            scope.passwordErrors = [];
            status = true;
          }
        }
        return status;
      };
    }
  }
}());

(function () {
  'use strict';

  angular
    .module('users')
    .directive('passwordVerify', passwordVerify);

  function passwordVerify() {
    var directive = {
      require: 'ngModel',
      scope: {
        passwordVerify: '='
      },
      link: link
    };

    return directive;

    function link(scope, element, attrs, ngModel) {
      var status = true;
      scope.$watch(function () {
        var combined;
        if (scope.passwordVerify || ngModel) {
          combined = scope.passwordVerify + '_' + ngModel;
        }
        return combined;
      }, function (value) {
        if (value) {
          ngModel.$validators.passwordVerify = function (password) {
            var origin = scope.passwordVerify;
            return (origin === password);
          };
        }
      });
    }
  }
}());

(function () {
  'use strict';

  // Users directive used to force lowercase input
  angular
    .module('users')
    .directive('lowercase', lowercase);

  function lowercase() {
    var directive = {
      require: 'ngModel',
      link: link
    };

    return directive;

    function link(scope, element, attrs, modelCtrl) {
      modelCtrl.$parsers.push(function (input) {
        return input ? input.toLowerCase() : '';
      });
      element.css('text-transform', 'lowercase');
    }
  }
}());

(function () {
  'use strict';

  // Authentication service for user variables

  angular
    .module('users.services')
    .factory('Authentication', Authentication);

  Authentication.$inject = ['$window'];

  function Authentication($window) {
    var auth = {
      user: $window.user
    };

    return auth;
  }
}());

(function () {
  'use strict';

  // Users service used for communicating with the users REST endpoint
  angular
    .module('users.services')
    .factory('PatientsService', PatientsService);

    PatientsService.$inject = ['$resource'];

  function PatientsService($resource) {
      return $resource('api/patients/:patientId', { patientId: '@_id'
      }, {
          update: {
              method: 'PUT'
          }
          /*query: {
              method: 'GET',
              isArray: false
          }*/
      });
  }

}());

(function () {
  'use strict';

  // Users service used for communicating with the users REST endpoint
  angular
    .module('users.services')
    .factory('ScratchersService', ScratchersService);

    ScratchersService.$inject = ['$resource'];

  function ScratchersService($resource) {
      return $resource('api/scratchers/:scratcherId', { scratcherId: '@_id'
      }, {
          update: {
              method: 'PUT'
          }
      });
  }

}());

(function () {
  'use strict';

  // Users service used for communicating with the users REST endpoint
  angular
    .module('users.services')
    .factory('UsersService', UsersService);

  UsersService.$inject = ['$resource'];

  function UsersService($resource) {
    return $resource('api/users', {}, {
      update: {
        method: 'PUT'
      }
    });
  }

  // TODO this should be Users service
  angular
    .module('users.admin.services')
    .factory('AdminService', AdminService);

  AdminService.$inject = ['$resource'];

  function AdminService($resource) {
    return $resource('api/users/:userId', {
      userId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());
