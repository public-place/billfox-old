var nodemailer = require('nodemailer');
var fs = require('fs');
var config = require('./config');
sendMail = function (toEmail, toUserName, body, inSubject, attachment, htmlBody) {
    var fromName, message, smtpTransport;
	smtpTransport = nodemailer.createTransport(config.mailer.options);
    fromName = config.mailer.fromName;
    message = {
        from: config.mailer.from,
        to: toEmail,
        subject: inSubject,
        headers: {
            "X-Laziness-level": 1000
        },
        html: htmlBody
    };

    if(attachment)    message.attachments = attachment;

	    return smtpTransport.sendMail(message, function (err) {
            console.log(err);
        if (!err) {
           return;
        } else {
          console.log('Success');
        }
			return
      });
};

module.exports.sendMail = sendMail;