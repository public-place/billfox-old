// route middleware to ensure user is logged in
var mailer = require('./sendmailer.js');
var config = require('./config');
var fs = require('fs');
var pdf = require('html-pdf');
var moment = require('moment');
var _ = require('lodash');


var options = {

    'format': 'A4',
    "border": "0",
    "margin": {
        "top": "0",
        "bottom": "0"
    },
    "footer": {
        "height": "1.5cm"
    }
}

function readFileTemplate(callback) {
    fs.readFile('./config/eliana.html', 'utf8', function(err, data) {
        if (err) {
            console.log(err);
        }
        callback(data);
    });
}

function setDateFormat(date, create) {
    if (date && create) {
        date = new Date(date);
        return moment(date).format('DD.MM.YYYY');
    } else if (date && !create) {
        date = new Date(date);
        return moment(date, 'MM-DD-YYYY').add(1, 'days').format('DD.MM.YYYY');
    } else {
        return '';
    }
}

sendEmail = function(patient, email) {
    readFileTemplate(function(htmlOutput) {

        var htmlBody = 'Hello, <br / >';
        htmlBody += '<p><b>Vorname : </b>' + patient.vorname || "" + '</p>';
        htmlBody += '<p><b>Nachname : </b> ' + patient.nachname || "" + ' </p>';
        if (patient.kindVornameNachname) htmlBody += '<p><b>Name Vorname : ' + patient.kindVornameNachname || "" + ' </b> </p>';
        htmlBody += '<p><b>Rechnungsart :</b>' + patient.rechnungsart || "" + '</p>';
        htmlBody += '<p><b>Amount : </b>' + format(patient.gesamt) + '</p>';
        var str = '<p>You got a new message from <br /></p>';
        str += '<p></p>';
        htmlOutput = htmlOutput.replace(/{{vm.patient.nachname}}/g, patient.nachname || '');
        htmlOutput = htmlOutput.replace(/{{vm.patient.vorname}}/g, patient.vorname || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.strabeHausnummer}}", patient.strabeHausnummer || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.plz}}", patient.plz || '');
        var add = patient.stadt ? patient.stadt : '';
        add += patient.address ? ' ' + patient.address : '';
        htmlOutput = htmlOutput.replace("{{vm.patient.stadt}}", add || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.rechnr}}", patient.rechnr || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.heute}}", setDateFormat(patient.heute) || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.anrede}}", patient.anrede || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.gebuh}}", patient.gebuh || '');
        htmlOutput = htmlOutput.replace("{{t}}", patient.ausfall || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.billNo}}", patient.billNo || '');
        if (patient.gebuHZiffern && (patient.gebuHZiffern == 'HP' || patient.gebuHZiffern == 'HP- Physio' || patient.gebuHZiffern == 'GKV'))
            htmlOutput = htmlOutput.replace("{{pageNo}}", "1/2");
        else
            htmlOutput = htmlOutput.replace("{{pageNo}}", "1/1");

        htmlOutput = htmlOutput.replace("{{vm.patient.created}}", setDateFormat(patient.created, 'created') || '');


        htmlOutput = htmlOutput.replace("{{vm.patient.behandlungsdatum_1}}", setDateFormat(patient.behandlungsdatum_1) || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.behandlungsdatum_2}}", setDateFormat(patient.behandlungsdatum_2) || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.behandlungsdatum_3}}", setDateFormat(patient.behandlungsdatum_3) || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.behandlungsdatum_4}}", setDateFormat(patient.behandlungsdatum_4) || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.behandlungsdatum_5}}", setDateFormat(patient.behandlungsdatum_5) || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.behandlungsdatum_6}}", setDateFormat(patient.behandlungsdatum_6) || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.behandlungsdatum_7}}", setDateFormat(patient.behandlungsdatum_7) || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.behandlungsdatum_8}}", setDateFormat(patient.behandlungsdatum_8) || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.behandlungsdatum_9}}", setDateFormat(patient.behandlungsdatum_9) || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.behandlungsdatum_10}}", setDateFormat(patient.behandlungsdatum_10) || '');

        htmlOutput = htmlOutput.replace("{{vm.patient.leistung1}}", patient.leistung1 || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.leistung2}}", patient.leistung2 || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.leistung3}}", patient.leistung3 || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.leistung4}}", patient.leistung4 || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.leistung5}}", patient.leistung5 || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.leistung6}}", patient.leistung6 || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.leistung7}}", patient.leistung7 || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.leistung8}}", patient.leistung8 || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.leistung9}}", patient.leistung9 || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.leistung10}}", patient.leistung10 || '');
        htmlOutput = htmlOutput.replace("{{vm.patient.orderNo}}", patient.orderNo || '');

        htmlOutput = htmlOutput.replace("{{vm.patient.betrag1}}", format(patient.betrag1));
        htmlOutput = htmlOutput.replace("{{vm.patient.betrag2}}", format(patient.betrag2));
        htmlOutput = htmlOutput.replace("{{vm.patient.betrag3}}", format(patient.betrag3));
        htmlOutput = htmlOutput.replace("{{vm.patient.betrag4}}", format(patient.betrag4));
        htmlOutput = htmlOutput.replace("{{vm.patient.betrag5}}", format(patient.betrag5));
        htmlOutput = htmlOutput.replace("{{vm.patient.betrag6}}", format(patient.betrag6));
        htmlOutput = htmlOutput.replace("{{vm.patient.betrag7}}", format(patient.betrag7));
        htmlOutput = htmlOutput.replace("{{vm.patient.betrag8}}", format(patient.betrag8));
        htmlOutput = htmlOutput.replace("{{vm.patient.betrag9}}", format(patient.betrag9));
        htmlOutput = htmlOutput.replace("{{vm.patient.betrag10}}", format(patient.betrag10));

        htmlOutput = htmlOutput.replace("{{vm.patient.gebuHZiffern}}", tableFormat(patient.gebuHZiffern, patient.billNo, setDateFormat(patient.created, 'created')));


        htmlOutput = htmlOutput.replace("{{vm.patient.gesamt}}", format(patient.gesamt));

        if (patient.rechnungsart === "Ausfallgebühr") {
            htmlOutput = htmlOutput.replace("{{vm.patient.gebDatumKind}}", '');
            htmlOutput = htmlOutput.replace("{{vm.patient.grund}}", '');
            htmlOutput = htmlOutput.replace("{{vm.patient.dia}}", '');
            htmlOutput = htmlOutput.replace("{{vm.patient.kind}}", '');
            htmlOutput = htmlOutput.replace("{{vm.patient.empfehlungsdatum}}", '.');
        } else {
            htmlOutput = htmlOutput.replace("{{vm.patient.grund}}", patient.grund || '');
            htmlOutput = htmlOutput.replace("{{vm.patient.gebDatumKind}}", setDateFormat(patient.gebDatumKind) || '');

            if ((patient.diagnose && patient.diagnose.length > 0) || (patient.diagnoseOther && patient.diagnoseOther.length > 0)) {
                console.log(patient.diagnose);
                if (patient.diagnose.indexOf('Andere :') > -1) {
                    patient.diagnose = patient.diagnose.replace('Andere :', '');
                }
                console.log(patient.diagnose);
                patient.diagnose = patient.diagnose.replace(/,/g, ', ');
                var diagnoseStr = '';
                diagnoseStr = patient.diagnose;
                if (patient.diagnoseOther) {
                    diagnoseStr += ' ' + patient.diagnoseOther;
                } else {
                    console.log("dsad");
                    //diagnoseStr = diagnoseStr.slice(0, -1);
                }
                diagnoseStr = diagnoseStr.replace(/\s+$/, '');
                htmlOutput = htmlOutput.replace("{{vm.patient.dia}}", diagnoseStr || '');
            } else {
                htmlOutput = htmlOutput.replace("{{vm.patient.dia}}", '');
            }
            htmlOutput = htmlOutput.replace("{{vm.patient.kind}}", patient.kind || '');
            //
            htmlOutput = htmlOutput.replace("{{vm.patient.empfehlungsdatum}}", patient.empfehlungsdatum && patient.empfehlungsdatum !== undefined ? ' ' + setDateFormat(patient.empfehlungsdatum) + '.' : '.');
        }

        //mailer.sendMail(config.email, 'Admin', htmlOutput, 'New message from '+name);
        pdf.create(htmlOutput, options).toStream(function(err, stream) {
            var newOne = stream;
            var attachment = [, { // stream as an attachment
                filename: patient.billNo + '-' + patient.nachname + '.pdf',
                content: newOne
            }];
            mailer.sendMail(email, 'Admin', '', 'New Form : ' + patient.nachname + ' ,' + patient.billNo, attachment, htmlBody);
        });


        pdf.create(htmlOutput, options).toStream(function(err, stream) {
            var newOne = stream;
            var attachment = [, { // stream as an attachment
                filename: patient.billNo + '-' + patient.nachname + '.pdf',
                content: newOne
            }];
            mailer.sendMail(email, 'Admin', '', 'New Form : ' + patient.nachname + ' ,' + patient.billNo, attachment, htmlBody);
        });



    });
}

function tableFormat(accountType, patientBillNo, patientCreated) {
    var str = '';
    console.log(accountType, patientBillNo, patientCreated);
    if (accountType == 'HP') {
        str += '   <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                               <tbody>  ' +
            '                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                       <th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-left: 16px; padding-right: 16px; text-align: left; width: 564px;">  ' +
            '                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                               <tbody>  ' +
            '                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                           <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                               <tbody>  ' +
            '                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                       <td height="150px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 150px; font-weight: normal; hyphens: auto; line-height: 150px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>  ' +
            '                                                                                   </tr>  ' +
            '                                                                               </tbody>  ' +
            '                                                                           </table>  ' +
            '   <table class="row" style="border-collapse: collapse; margin-bottom: 20px; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                               <tbody>  ' +
            '                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                       <th class="small-12 large-3 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 25%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Rechnungsnummer</strong><br>' + patientBillNo +
            '                                                                                                                           </p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                       <th class="small-12 large-3 columns" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 25%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Datum</strong><br>' + patientCreated +
            '                                                                                                                           </p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                       <th class="small-12 large-3 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 25%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Seite</strong><br>2/2' +
            '                                                                                                                           </p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                   </tr>  ' +
            '                                                                                               </tbody>  ' +
            '                                                                                          </table>  ' +
            '<table class="callout" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                               <tbody>  ' +
            '                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                       <th class="callout-inner2 secondary" style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"><strong>Übersicht der GebüH Ziffern:</strong></p>  ' +
            '   <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                               <tbody>  ' +
            '                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                       <td height="50px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 50px; font-weight: normal; hyphens: auto; line-height: 50px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>  ' +
            '                                                                                   </tr>  ' +
            '                                                                               </tbody>  ' +
            '                                                                          </table>  ' +
            '                                                                           <table class="callout" style="Margin-bottom: 16px; border-collapse: collapse; border-spacing: 0; margin-bottom: 16px; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                               <tbody>  ' +
            '                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                       <th class="small-12 large-2 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 16.66667%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Ziffer</strong></p>  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;">  ' +
            '                                                                                                                               1<br>5<br>20.1<br>20.2<br>20.5<br>20.7<br>20.8<br>34.2<br>35.2<br>35.3<br>35.4<br>35.6<br>39.6  ' +
            '                                                                                                                           </p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                       <th class="small-12 large-8 columns" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 66.66667%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Beschreibung</strong></p>  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;">Untersuchung<br>Beratung, auch telefonisch<br>Atemther. Behandlungsverfahren<br>Nervenpunktmassage<br>Großmassage<br>Aufbauübungen  ' +
            '                                                                                                                               und Mobilisation<br>Einreibung zu therapeut. Zwecken<br>Chiropraktischer Eingriff gezielt<br>Osteopathie Schulter und BWS<br>Osteopathie  ' +
            '                                                                                                                               Extremitäten  ' +
            '                                                                                                                               <br>Osteopathie Schlüsselbein/Knie  ' +
            '                                                                                                                               <br>Osteopathie Finger/Zehen<br>Heizsonne  ' +
            '                                                                                                                           </p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                       <th class="small-12 large-1 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 8.33333%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p class="text-right" style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: right;"> <strong>Betrag</strong></p>  ' +
            '                                                                                                                           <p class="text-right" style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: right;">12,50 €<br>4,00 €<br>10,00 €<br>4,00 €<br>6,00 €<br>6,50 €<br>6,00 €<br>17,00 €<br>21,00 €<br>10,00 €<br>12,00 €<br>11,00 €<br>4,00 €' +
            '                                                                                                                           </p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                   </tr>  ' +
            '                                                                                               </tbody>  ' +
            '                                                                                           </table>  ' +
            '                                                                                       </th>  ' +
            '                                                                                       <th class="expander" style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0;"></th>  ' +
            '                                                                                   </tr>  ' +
            '                                                                               </tbody>  ' +
            '                                                                           </table>  ' +
            '                                                                       </th>  ' +
            '                                                                   </tr>  ' +
            '                                                               </tbody>  ' +
            '                                                           </table>  ' +
            '                                                       </th>  ' +
            '                                                   </tr>  ' +
            '                                               </tbody>  ' +
            '                                          </table>  ';
    } else if (accountType == 'GKV') {
        str += '   <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">  ' +
            '               <tbody>  ' +
            '                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                       <th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-left: 16px; padding-right: 16px; text-align: left; width: 564px;">  ' +
            '                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                               <tbody>  ' +
            '                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                           <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                               <tbody>  ' +
            '                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                       <td height="150px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 150px; font-weight: normal; hyphens: auto; line-height: 150px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>  ' +
            '                                                   </tr>  ' +
            '                                               </tbody>  ' +
            '                                           </table>  ' +
            '   <table class="row" style="border-collapse: collapse; margin-bottom: 20px; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                               <tbody>  ' +
            '                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                       <th class="small-12 large-3 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 25%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Rechnungsnummer</strong><br>' + patientBillNo +
            '                                                                                                                           </p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                       <th class="small-12 large-3 columns" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 25%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Datum</strong><br>' + patientCreated +
            '                                                                                                                           </p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                       <th class="small-12 large-3 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 25%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Seite</strong><br>2/2' +
            '                                                                                                                           </p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                   </tr>  ' +
            '                                                                                               </tbody>  ' +
            '                                                                                          </table>  ' +
            '                                           <table class="callout" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                               <tbody>  ' +
            '                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                       <th class="callout-inner2 secondary" style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"><strong>Übersicht der GebüH Ziffern:</strong></p>  ' +
            '                                                           <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                               <tbody>  ' +
            '                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                       <td height="50px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 50px; font-weight: normal; hyphens: auto; line-height: 50px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>  ' +
            '                                                                   </tr>  ' +
            '                                                               </tbody>  ' +
            '                                                           </table>  ' +
            '                                                           <table class="callout" style="Margin-bottom: 16px; border-collapse: collapse; border-spacing: 0; margin-bottom: 16px; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                               <tbody>  ' +
            '                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                       <th class="small-12 large-2 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 16.66667%;">  ' +
            '                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                               <tbody>  ' +
            '                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Ziffer</strong></p>  ' +
            '                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;">  ' +
            '                                                                                           1<br>4<br>35.1<br>35.2<br>35.3<br>35.4  ' +
            '                                                                                           </p>  ' +
            '                                                                                           </th>  ' +
            '                                                                                           </tr>  ' +
            '                                                                                           </tbody>  ' +
            '                                                                                           </table>  ' +
            '                                                                                           </th>  ' +
            '                                                                                           <th class="small-12 large-8 columns" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 66.66667%;">  ' +
            '                                                                                               <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                   <tbody>  ' +
            '                                                                                                       <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                           <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                               <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Beschreibung</strong></p>  ' +
            '                                                                                                               <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;">osteopathische Behandlung<br>osteopathische Beratung<br>Osteopathie viszeral <br>Osteopathie Schulter/ Wirbelsäule<br>Osteopathie Extremitäten<br>Osteopathie Schlüsselbein/Knie                                                                                                                  </p>  ' +
            '                                                                                                               </th>  ' +
            '                                                                                                               </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                               </table>  ' +
            '                                                                                                               </th>  ' +
            '                                                                                                               <th class="small-12 large-1 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 8.33333%;">  ' +
            '                                                                                                                   <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                                       <tbody>  ' +
            '                                                                                                                           <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                               <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                                   <p class="text-right" style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: right;"> <strong>Betrag</strong></p>  ' +
            '                                                                                                                                   <p class="text-right" style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: right;">5,00 €<br>5,00 €<br>30,00 €<br>35,00 €<br>25,00 €<br>25,00 €  ' +
            '                                                                                                                                   </p>  ' +
            '                                                                                                                                   </th>  ' +
            '                                                                                                                                   </tr>  ' +
            '                                                                                                                                   </tbody>  ' +
            '                                                                                                                                   </table>  ' +
            '                                                                                                                                   </th>  ' +
            '                                                                                                                               </tr>  ' +
            '                                                                                                                           </tbody>  ' +
            '                                                                                                                       </table>  ' +
            '                                                                                                                   </th>  ' +
            '                                                                                                                   <th class="expander" style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0;"></th>  ' +
            '                                                                                                               </tr>  ' +
            '                                                                                                           </tbody>  ' +
            '                                                                                                       </table>  ' +
            '                                                                                                   </th>  ' +
            '                                                                                               </tr>  ' +
            '                                                                                           </tbody>  ' +
            '                                                                                       </table>  ' +
            '                                                                                   </th>  ' +
            '                                                                               </tr>  ' +
            '                                                                           </tbody>  ' +
            '                                                                       </table> ';
    } else if (accountType === "HP- Physio") {
        str += '   <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                               <tbody>  ' +
            '                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                       <th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 0; padding-left: 16px; padding-right: 16px; text-align: left; width: 564px;">  ' +
            '                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                               <tbody>  ' +
            '                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                           <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                               <tbody>  ' +
            '                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                       <td height="150px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 150px; font-weight: normal; hyphens: auto; line-height: 150px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>  ' +
            '                                                                                   </tr>  ' +
            '                                                                               </tbody>  ' +
            '                                                                           </table>  ' +
            '   <table class="row" style="border-collapse: collapse; margin-bottom: 20px; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                               <tbody>  ' +
            '                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                       <th class="small-12 large-3 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 25%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Rechnungsnummer</strong><br>' + patientBillNo +
            '                                                                                                                           </p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                       <th class="small-12 large-3 columns" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 25%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Datum</strong><br>' + patientCreated +
            '                                                                                                                           </p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                       <th class="small-12 large-3 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 25%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Seite</strong><br>2/2' +
            '                                                                                                                           </p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                   </tr>  ' +
            '                                                                                               </tbody>  ' +
            '                                                                                          </table>  ' +
            '                                                                           <table class="callout" style="Margin-bottom: 16px; border-collapse: collapse; border-spacing: 0; margin-bottom: 16px; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                               <tbody>  ' +
            '                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                       <th class="callout-inner2 secondary" style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"><strong>Übersicht der GebüH Ziffern:</strong></p>  ' +
            '   <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                               <tbody>  ' +
            '                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                       <td height="50px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 50px; font-weight: normal; hyphens: auto; line-height: 50px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>  ' +
            '                                                                                   </tr>  ' +
            '                                                                               </tbody>  ' +
            '                                                                          </table>  ' +
            '                                                                                           <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                               <tbody>  ' +
            '                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                       <th class="small-12 large-2 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 16.66667%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Ziffer</strong></p>  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;">A1<br>3<br>4<br>5<br>17.1<br>A18.2<br>20.1<br>20.2<br>20.3<br>20.4<br>20.6<br>20.7<br>20.8<br>39.6</p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                       <th class="small-12 large-8 columns" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 66.66667%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;"> <strong>Beschreibung</strong></p>  ' +
            '                                                                                                                           <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;">Untersuchung; Anmerkung: erhöhter Zeitaufwand, da umfangreiche Symptomatik<br>Kurze Information<br>Eingehende Beratung<br>Beratung,  ' +
            '                                                                                                                               auch telefonisch<br>Neurologische Untersuchung  ' +
            '                                                                                                                               <br>Krankengymnastik<br>Atemtherapie<br>Nervenpunktmassage<br>Bindegewebsmassage<br>Teilmassage<br>Sondermassage<br>Aufbauübungen  ' +
            '                                                                                                                               und Mobilisation  ' +
            '                                                                                                                               <br>Einreibung zu therapeutischen Zwecken<br>Wärmeanwendung</p>  ' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                       <th class="small-12 large-1 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0 !important; padding-right: 0 !important; text-align: left; width: 8.33333%;">  ' +
            '                                                                                                           <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">  ' +
            '                                                                                                               <tbody>  ' +
            '                                                                                                                   <tr style="padding: 0; text-align: left; vertical-align: top;">  ' +
            '                                                                                                                       <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">  ' +
            '                                                                                                                           <p class="text-right" style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: right;"> <strong>Betrag</strong></p>  ' +
            '                                                                                                                           <p class="text-right" style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: right;">18,50 €<br>3,00 €<br>18,00 €<br>4,80 €<br>19,50 €<br>13,50 €<br>8,20 €<br>6,50 €<br>6,00 €<br>4,80 €<br>8,00 €<br>5,00 €<br>5,20 €<br>4,80 €</p>' +
            '                                                                                                                       </th>  ' +
            '                                                                                                                   </tr>  ' +
            '                                                                                                               </tbody>  ' +
            '                                                                                                           </table>  ' +
            '                                                                                                       </th>  ' +
            '                                                                                                   </tr>  ' +
            '                                                                                               </tbody>  ' +
            '                                                                                           </table>  ' +
            '                                                                                       </th>  ' +
            '                                                                                       <th class="expander" style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0;"></th>  ' +
            '                                                                                   </tr>  ' +
            '                                                                               </tbody>  ' +
            '                                                                           </table>  ' +
            '                                                                       </th>  ' +
            '                                                                   </tr>  ' +
            '                                                               </tbody>  ' +
            '                                                           </table>  ' +
            '                                                       </th>  ' +
            '                                                   </tr>  ' +
            '                                               </tbody>  ' +
            '                                          </table>  ';
    } else
        str = '';


    return str;
}

function format(str) {
    if (!str) return str = '';
    str = Number(str);
    str = str.toFixed(2);
    str = str.toString();
    if (str.indexOf('.') > -1) {
        str = str.replace('.', ',')
    } else {
        str = str + ',00';
    }

    str = str + ' €';
    return str;
}

module.exports.sendEmail = sendEmail;