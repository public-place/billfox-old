'use strict';

module.exports = {
    client: {
        lib: {
            css: [
                // bower:css
                'public/lib/angular-material/angular-material.min.css',
                'public/lib/md-steppers/dist/md-steppers.min.css',
                'public/lib/font-awesome/css/font-awesome.min.css',
                'public/lib/toastr/toastr.min.css',
                'public/lib/css/style.css',
                'public/lib/datepicker.css'
            ],
            js: [
                // bower:js
                'public/lib/jquery/dist/jquery.js',
                'public/lib/angular/angular.js',
                'public/lib/moment/moment.js',
                'public/lib/angular-resource/angular-resource.js',
                'public/lib/angular-animate/angular-animate.js',
                'public/lib/angular-aria/angular-aria.min.js',
                'public/lib/angular-messages/angular-messages.js',
                'public/lib/angular-sanitize/angular-sanitize.min.js',
                'public/lib/angular-ui-select/dist/select.min.js',
                'public/lib/angular-ui-router/release/angular-ui-router.js',
                'public/lib/angular-file-upload/dist/angular-file-upload.js',
                'public/lib/angular-material/angular-material.min.js',
                'public/lib/angular-material-icons/angular-material-icons.min.js',
                'public/lib/lodash/lodash.min.js',
                'public/lib/md-data-table/dist/md-data-table.js',
                'public/lib/md-data-table/dist/md-data-table-templates.js',
                'public/lib/md-steppers/dist/md-steppers.min.js',
                'public/lib/slick-carousel/slick/slick.js',
                'public/lib/angular-slick/dist/slick.js',
                'public/lib/toastr/toastr.min.js',
                'public/lib/angular-datetime/dist/datetime.js',
                'public/lib/datepicker.js',
                'public/lib/md-collection-pagination/dist/dist.min.js'
                // endbower
            ]
        },
        css: [
            'modules/*/client/css/*.css'
        ],
        js: [
            'modules/core/client/app/config.js',
            'modules/core/client/app/init.js',
            'modules/*/client/*.js',
            'modules/*/client/**/*.js'
        ],
        img: [
            'modules/**/*/img/**/*.jpg',
            'modules/**/*/img/**/*.png',
            'modules/**/*/img/**/*.gif',
            'modules/**/*/img/**/*.svg'
        ],
        views: ['modules/*/client/views/**/*.html'],
        templates: ['build/templates.js']
    }
};
