(function () {
  'use strict';

  // Users service used for communicating with the users REST endpoint
  angular
    .module('users.services')
    .factory('ScratchersService', ScratchersService);

    ScratchersService.$inject = ['$resource'];

  function ScratchersService($resource) {
      return $resource('api/scratchers/:scratcherId', { scratcherId: '@_id'
      }, {
          update: {
              method: 'PUT'
          }
      });
  }

}());
