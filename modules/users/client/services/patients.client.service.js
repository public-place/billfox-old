(function () {
  'use strict';

  // Users service used for communicating with the users REST endpoint
  angular
    .module('users.services')
    .factory('PatientsService', PatientsService);

    PatientsService.$inject = ['$resource'];

  function PatientsService($resource) {
      return $resource('api/patients/:patientId', { patientId: '@_id'
      }, {
          update: {
              method: 'PUT'
          }
          /*query: {
              method: 'GET',
              isArray: false
          }*/
      });
  }

}());
