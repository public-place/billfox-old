(function () {
  'use strict';

  angular
    .module('users')
    .controller('ScratcherController', ScratcherController);

    ScratcherController.$inject = ['$scope', '$stateParams', '$http', '$location', 'Authentication', 'FileUploader', '$window', '$timeout', 'ScratchersService'];

  function ScratcherController($scope, $stateParams, $http, $location, Authentication, FileUploader, $window, $timeout, ScratchersService ) {
    var vm = this;
    $scope.scratchers = [];
	  vm.init = init;
      vm.create = create;
      vm.remove = remove;
      vm.update = update;
      vm.getOne = getOne;
	  vm.find = find;
	  vm.isOpen = false;

      vm.authentication = Authentication;
      if (!vm.authentication.user) {
          $location.path('/');
      }

      function getDay(index) {
          var weekday= ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday","Friday","Saturday"];
          return weekday[index];
      }
		
		function init() {
			vm.scratcher = {};
            var stateDate = new Date();
            vm.scratcher.announceDay = getDay(stateDate.getDay());
            stateDate.setHours(0);
            stateDate.setMinutes(30);
            vm.scratcher.endTime = endDate;
            var endDate = new Date();
            endDate.setHours(23);
            endDate.setMinutes(55);

			vm.scratcher.startTime = stateDate;
			vm.scratcher.endTime = endDate;
			
		}
		init();
		find();
      function getOne(scratcher) {
          vm.scratcher = scratcher;
      }


      function beforeAddAndUpdate() {
          var temp =  angular.copy(vm.scratcher);
          temp.tempStartHour = new Date(temp.startTime).getHours();
          temp.tempStartMinutes = new Date(temp.startTime).getMinutes();

          temp.tempEndHour = new Date(temp.endTime).getHours();
          temp.tempEndMinutes = new Date(temp.endTime).getMinutes();

          temp.startTime = new Date(temp.startTime).getTime();
          temp.endTime = new Date(temp.endTime).getTime();

          return temp;
      }

      // Create new Scratcher
      function create() {
          // Create new Scratcher object
          var temp =  beforeAddAndUpdate();
          var scratcher = new ScratchersService (temp);

          // Redirect after save
          scratcher.$save(function(response) {
              //vm.scratchers.push(response);
              toastr.success('Scratcher successfully added');
              init();
              find();

          }, function(errorResponse) {
              toastr.error('Error : '+errorResponse.data.message);
          });
      };

      // Remove existing Scratcher
      function remove(scratcher){
          if ( scratcher ) {
              scratcher.$remove();

              for (var i in vm.scratchers) {
                  if (vm.scratchers [i] === scratcher) {
                      vm.scratchers.splice(i, 1);
                  }
              }
          }
      };

      // Update existing Scratcher
      function update() {
		var temp =  beforeAddAndUpdate();
          var scratcher = new ScratchersService (temp);
          scratcher.$update(function() {
               toastr.success('Scratcher successfully updated');
          }, function(errorResponse) {
              toastr.error('Error : '+errorResponse.data.message);
          });
      };



      // Find a list of Scratchers
      function find() {
         $http.get('/api/scratchers').success(function(data){
             console.log(data);
             vm.scratchers = data.result;
             vm.resultHeader = data.scratcherCount;
         }).error(function(){

         });
      };

      

      // Find existing Scratcher
      $scope.findOne = function(id) {
          $scope.scratcher = ScratchersService.get({
              scratcherId: id
          });
      };

  }
}());
