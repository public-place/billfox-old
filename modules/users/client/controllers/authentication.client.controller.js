(function () {
  'use strict';

  angular
    .module('users')
    .controller('AuthenticationController', AuthenticationController);

  AuthenticationController.$inject = ['$scope', '$state', '$http', '$location', '$window', 'Authentication'];

  function AuthenticationController($scope, $state, $http, $location, $window, Authentication) {
    var vm = this;

    $scope.authentication = Authentication;
      $scope.signCredentials = {};
      $scope.signin = signin;
    vm.callOauthProvider = callOauthProvider;

    // Get an eventual error defined in the URL query string:
    vm.error = $location.search().err;

    // If user is signed in then redirect back home
    if ($scope.authentication.user) {
      $location.path('/');
    }

    function signin(isValid) {
      vm.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.userForm');
      }

      $http.post('/api/auth/signin', $scope.signCredentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;
          toastr.info('Login successfully');
        // And redirect to the previous or home page
          $location.path('/');
      }).error(function (response) {
          toastr.error('Error : '+response.message);
      });
    }

    // OAuth provider request
    function callOauthProvider(url) {
      if ($state.previous && $state.previous.href) {
        url += '?redirect_to=' + encodeURIComponent($state.previous.href);
      }

      // Effectively call OAuth authentication route:
      $window.location.href = url;
    }
  }
}());
