(function () {
  'use strict';

  angular
    .module('users')
    .controller('EditProfileController', EditProfileController);

  EditProfileController.$inject = ['$scope', '$http', '$location', 'UsersService', 'Authentication', 'FileUploader', '$window', '$timeout'];

  function EditProfileController($scope, $http, $location, UsersService, Authentication, FileUploader, $window, $timeout ) {
    var vm = this;
    vm.user = Authentication.user;
    vm.goal = {};
    vm.imageType = { type : 'image'};
    vm.updateGoalInfo = updateGoalInfo;
    vm.updateUserProfile = updateUserProfile;

      function clear() {
          vm.actionSelection = {};
          vm.actionSelection.isProfileEdit = false;
          vm.actionSelection.isGoalEdit = false;
      }
      clear();

    // Update a user profile
    function updateUserProfile(isValid, formType) {
        console.log(isValid);
      vm.success = vm.error = null;
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.userForm');
        return false;
      }

      var user = new UsersService(vm.user);

      user.$update(function (response) {
        toastr.success(formType +' successfully updated');
        $scope.$broadcast('show-errors-reset', 'vm.userForm');
        clear();
        Authentication.user = response;
      }, function (response) {
        vm.error = response.data.message;
      });



    }


      vm.imageURL = vm.user.profileImageURL;
      vm.uploadProfilePicture = uploadProfilePicture;

      vm.cancelUpload = cancelUpload;
      // Create file uploader instance
      vm.uploader = new FileUploader({
          alias: 'newProfilePicture',
          onAfterAddingFile: onAfterAddingFile,
          onBeforeUploadItem : onBeforeUploadItem,
          onSuccessItem: onSuccessItem,
          onErrorItem: onErrorItem
      });

      //-----------------------------------

      /* Get User Goal Info */

      getUserGoalInfo();
      function getUserGoalInfo(){
          $http.get('/api/goals/'+vm.user.goal).success(function (response) {
              console.log(response);
              vm.goal = response;
              vm.goalImageUrl = vm.goal.imageURL;
          }).error(function (response) {
              // Show user error message and clear form
              vm.credentials = null;
              vm.error = response.message;
          });
      }

      function updateGoalInfo() {
          console.log(vm.goal);
          $http.put('/api/goals/'+vm.user.goal, vm.goal).success(function (response) {
              vm.goal = response;
              vm.goalImageUrl = vm.goal.imageURL;
              vm.actionSelection.isGoalEdit = false;
              toastr.success('Goal info successfully updated');
          }).error(function (response) {
              // Show user error message and clear form
              vm.credentials = null;
              vm.error = response.message;
          });
      }

      $scope.availableCategories = ['Dieren', 'Gezondheid', 'Kinderen', 'Mensen', 'Natuur', 'Religieus'];







      // Set file uploader image filter
      vm.uploader.filters.push({
          name: 'imageFilter',
          fn: function (item, options) {
              var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
              return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
          }
      });

      // Called after the user selected a new picture file
      function onAfterAddingFile(fileItem) {
          if ($window.FileReader) {
              var fileReader = new FileReader();
              fileReader.readAsDataURL(fileItem._file);

              fileReader.onload = function (fileReaderEvent) {
                  $timeout(function () {
                      if(vm.imageType.type == 'image') {
                          vm.imageURL = fileReaderEvent.target.result;
                      }

                      if(vm.imageType.type == 'goal') {
                          vm.goalImageUrl  = fileReaderEvent.target.result;
                      }

                  }, 0);
              };
          }
      }

      function onBeforeUploadItem(item) {
          item.url = 'api/users/picture?imageType='+vm.imageType.type;
      }

          // Called after the user has successfully uploaded a new picture
      function onSuccessItem(fileItem, response, status, headers) {
          // Show success message
          toastr.success('image successfully updated');
          if(vm.imageType.type == 'image'){
              vm.user = Authentication.user = response;
          }else {
              vm.goal.imageURL =  response.imageUrl;
              vm.goalImageUrl = response.imageUrl;
          }

          // Populate user object


          // Clear upload buttons
          cancelUpload();
      }

      // Called after the user has failed to uploaded a new picture
      function onErrorItem(fileItem, response, status, headers) {
          // Clear upload buttons
          cancelUpload();

          // Show error message
          vm.error = response.message;
      }

      // Change user profile picture
      function uploadProfilePicture() {
          // Clear messages
          vm.success = vm.error = null;
          vm.uploader.queue[0].formData = vm.imageType.type;
          // Start upload
          vm.uploader.uploadAll();
      }

      // Cancel the upload process
      function cancelUpload() {
          vm.uploader.clearQueue();
          vm.imageURL = vm.user.profileImageURL;
          vm.goalImageUrl = vm.goal.imageURL;
      }
  }
}());
