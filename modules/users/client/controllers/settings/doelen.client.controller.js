(function () {
  'use strict';

  angular
    .module('users')
    .controller('DoelenController', GoalController);

    GoalController.$inject = ['$scope', '$stateParams', '$http', '$location', 'Authentication', 'FileUploader', '$window', '$timeout'];

  function GoalController($scope, $stateParams, $http, $location, Authentication, FileUploader, $window, $timeout ) {
    var vm = this;
    vm.dolens = [];
    vm.dolensCount = [];
    $scope.authentication = Authentication;


      function getDoelen() {
              $http.get('/api/goals/doelen').success(function (response) {
                  vm.dolens = response.result;
                  vm.dolensCount = response.goalCount;
              }).error(function (response) {
                  // Show user error message and clear form
                  vm.credentials = null;
                  vm.error = response.message;
              });
      }

      getDoelen();

  }
}());
