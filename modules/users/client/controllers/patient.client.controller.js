(function () {
  'use strict';

  angular
    .module('users')
    .controller('PatientController', PatientController);

    PatientController.$inject = ['$scope', '$stateParams', '$http', '$location', 'Authentication', 'FileUploader', '$window', '$timeout', 'PatientsService', '$mdDialog'];

  function PatientController($scope, $stateParams, $http, $location, Authentication, FileUploader, $window, $timeout, PatientsService, $mdDialog) {
    var vm = this;
      vm.patients = [];
      vm.patient  = {};
      vm.list = [1,2,3,4,5,6,7,8,9,10]
	  vm.find = find;
      vm.authentication = Authentication;
      if (!vm.authentication.user) {
          $location.path('/admin/login');
      }

      $scope.dialog = { 'result' : 'menk.osteo@gmail.com'};


      vm.showPrompt = function(ev, patient) {
          // Appending dialog to document.body to cover sidenav in docs app
          var confirm = $mdDialog.prompt()
              .title('Enter email address')
              .placeholder('menk.osteo@gmail.com')
              //.initialValue('menk.osteo@gmail.com')
              .ariaLabel('Email')
              .targetEvent(ev)
              .ok('Okay!')
              .cancel('Cancel');

          $mdDialog.show(confirm).then(function(result) {
              var patients = [];

              for(var k=0; k < vm.patients.length; k++){
                    if(vm.patients[k].isSelect) patients.push(vm.patients[k]._id);
              }
              console.log("========>>>", patients);

              if(!result)   result = 'menk.osteo@gmail.com';
              $http.post('/api/sendEmail', {email : result, patients : patients}).success(function(data){
                  console.log(data);
                  vm.scratchers = data.result;
                  vm.resultHeader = data.scratcherCount;
              }).error(function(){

              });

          }, function() {
              $scope.status = 'You didn\'t name your dog.';
          });
      };

      $scope.setToAllFn = function(status){
         for(var k=0; k < vm.patients.length; k++){
             vm.patients[k].isSelect = status;
         }
      }

      if (vm.authentication.user && vm.authentication.user.roles && vm.authentication.user.roles[0] !== 'admin') {
          $location.path('/');
      }

      vm.isOpen = false;
        if( $stateParams.patientId){
            findOne($stateParams.patientId);
        }

      function findOne(id) {
          vm.patient = PatientsService.get({
              patientId: id
          });
      };

      vm.formatPrintf = function(str, andre) {
            if(str) {
                str = str.replace('Andere :', '');
                str += andre;
            }
            return str;
      }

      vm.format = function(str) {
          if(!str)    return str = '';
          str = Number(str);
          str = str.toFixed(2);
          str = str.toString();
          if(str.indexOf('.') > -1){
              str.replace('.',',')
          }else {
              str = str+',00';
          }

          str = str+' €';
          return str;
      }

      $scope.paging = {
          page: 1,
          onPageChanged: find
      };

      function find(q) {
          $http.get('/api/patients', {
              params: { page: $scope.paging.page, q : q }
          }).success(function(data){
              vm.patients = data.result;
              if(data.count)    $scope.paging.total = Math.ceil(data.count / 10);
              if(data.total && data.total[0] && data.total[0].total) vm.getTotal = data.total[0].total;
          }).error(function(){

          });
      };


      vm.update = function(ev, result, isCheckList) {
          result = new PatientsService(result);
          if(!result.isPaid && isCheckList){
              var confirm = $mdDialog.confirm()
                  .title('do u wirklich deaktivieren wollen?')
                  .targetEvent(ev)
                  .ok('Ok')
                  .cancel('Abbrechen');

              $mdDialog.show(confirm).then(function() {
                  result.$update(function(response) {
                      toastr.success('Document successfully update');
                      find();
                  }, function(errorResponse) {
                      toastr.error('Error : '+errorResponse.data.message);
                  });
              }, function() {
                  result.isPaid = true;
              });

          }else {
              result.$update(function(response) {
                  toastr.success('Document successfully update');
                  find();
              }, function(errorResponse) {
                  toastr.error('Error : '+errorResponse.data.message);
              });
          }
      }

      if(!$stateParams.patientId) {
          find();
      }

      vm.showConfirm = function(ev, patient,index) {
          // Appending dialog to document.body to cover sidenav in docs app
          var confirm = $mdDialog.confirm()
              .title('Möchtest du den Eintrag löschen?')
              .textContent("Was weck ist, ist wech.")
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('Ok')
              .cancel('Abbrechen');

          $mdDialog.show(confirm).then(function() {
			  patient = new PatientsService(patient);	
              patient.$remove();
              vm.patients.splice(index, 1);
              toastr.success('Document successfully Deleted');
          }, function() {
              $scope.status = 'You decided to keep your debt.';
          });
      };

  }
}());
