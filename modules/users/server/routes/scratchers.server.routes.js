'use strict';

/**
 * Module dependencies
 */
//var scratchersPolicy = require('../policies/scratchers.server.policy'),
  var scratchers = require('../controllers/scratchers.server.controller');

module.exports = function (app) {
  // Articles collection routes
    //.all(scratchersPolicy.isAllowed)
  app.route('/api/scratchers')
    .get(scratchers.list)
    .post(scratchers.create);


    app.route('/api/scratchers/isAlready').get(scratchers.getIsAlready);

    app.route('/api/v1/scratchcard/:id').get(scratchers.getScratcherCard);
  // Single scratcher routes
  app.route('/api/scratchers/:scratcherId')
      //.all(scratchersPolicy.isAllowed)
    .get(scratchers.read)
    .put(scratchers.update)
    .delete(scratchers.delete);

  // Finish by binding the scratcher middleware
  app.param('scratcherId', scratchers.scratcherByID);
};
