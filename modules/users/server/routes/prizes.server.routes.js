'use strict';

/**
 * Module dependencies
 */
//var prizesPolicy = require('../policies/prizes.server.policy'),
  var prizes = require('../controllers/prizes.server.controller');

module.exports = function (app) {

    app.route('/api/patients')
        .get(prizes.list)
        .post(prizes.create);

    app.route('/api/sendEmail')
        .post(prizes.sendEmailApi);

    app.route('/api/getCity/:pincode')
        .get(prizes.pincode);

    app.route('/api/update/patients/:patientId')
        //.all(prizesPolicy.isAllowed)
        .get(prizes.updateStatus);

/*
  // Articles collection routes
    //.all(prizesPolicy.isAllowed)


    app.route('/api/prizes/doelen')
        .get(prizes.doelen);

    app.route('/api/prize/getPublicInfo/:url').get(prizes.getPublicInfo);

    app.route('/api/prize/getRandomNumber').get(prizes.getRandomNumber);

    app.route('/api/v1/prizes/:prizeId').get(prizes.getPrizeInfo);

  // Single prize routes
  app.route('/api/prizes/:prizeId')
      //.all(prizesPolicy.isAllowed)
    .get(prizes.read)
    .put(prizes.update)
    .delete(prizes.delete);

  // Finish by binding the prize middleware

*/
    app.route('/api/patients/:patientId')
        //.all(prizesPolicy.isAllowed)
        .get(prizes.read)
        .put(prizes.update)
        .delete(prizes.delete);
    app.param('patientId', prizes.patientByID);
};
