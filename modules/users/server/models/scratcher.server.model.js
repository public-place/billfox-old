'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Scratcher Schema
 */
var ScratcherSchema = new Schema({
    email: {
        type: String
    },
    id: {
        type: String,
        unique: 'Id already exists',
        required: 'Id must required',
        lowercase: true,
        trim: true
    },
    isPrizeCollected : {
        type : Boolean,
        default : false
    },
    updated: {
        type: Date
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    prizeId: {
        type: Schema.ObjectId,
        ref: 'Prize'
    }
});

mongoose.model('Scratcher', ScratcherSchema);
