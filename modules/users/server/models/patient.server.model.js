'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Patient Schema
 */
var PatientSchema = new Schema({
    heute : {
        type: Number
    },
    nachname: {
        type: String
    },
    address: {
        type: String
    },
    vorname : {
        type: String
    },
    anrede: {
        type: String
    },
    strabeHausnummer : {
        type: String
    },
    plz : {
        type: String
    },
    B1 : {
        type: Number
    },
    description : {
        type: String
    },
    isPaid : {
        type: Boolean,
        default : false
    },
    B2 : {
        type: Number
    },
    B3 : {
        type: Number
    },
    L1 : {
        type: String
    },
    L2 : {
        type: String
    },
    L3 : {
        type: String
    },
    stadt : {
        type: String
    },
    kindVornameNachname: {
        type: String
    },
    gebDatumKind : {
        type: Number
    },
    orderNo : {
        type: Number
    },
    billNo : {
        type: String
    },
    rechnungsart: {
        type: String
    },
    grund  : {
        type: String
    },
    ausfall : {
        type: String
    },
    gebuHZiffern : {
        type: String
    },
    grundMapping : {
        type : String
    },
    gebuh : {
        type: String
    },
    stadtOther  : {
        type: String
    },
    diagnoseOther : {
        type: String
    },
    rechnr : {
        type: String
    },
    kind : {
        type: String
    },
    ba : {
        type: String
    },
    bb : {
        type: String
    },
    gesamt : {
        type : Number
    },
    summe : {
        type : Number
    },
    empfehlungsdatum  : {
        type : Number
    },
    leistung1  : {
        type : String
    },
    leistung2  : {
        type : String
    },
    leistung3  : {
        type : String
    },
    leistung4  : {
        type : String
    },
    leistung5  : {
        type : String
    },
    leistung6  : {
        type : String
    },
    leistung7  : {
        type : String
    },
    leistung8  : {
        type : String
    },
    leistung9  : {
        type : String
    },
    leistung10  : {
        type : String
    },
    betrag1  : {
        type : Number
    },
    betrag2  : {
        type : Number
    },
    betrag3  : {
        type : Number
    },
    betrag4  : {
        type : Number
    },
    betrag5  : {
        type : Number
    },
    betrag6  : {
        type : Number
    },
    betrag7  : {
        type : Number
    },
    betrag8  : {
        type : Number
    },
    betrag9  : {
        type : Number
    },
    betrag10  : {
        type : Number
    },

    diagnose: {
        type: String
    },
    behandlungsdatum_1 : {
        type: Number
    },
    behandlungsdatum_2 : {
        type: Number
    },
    behandlungsdatum_3 : {
        type: Number
    },
    behandlungsdatum_4 : {
        type: Number
    },
    behandlungsdatum_5 : {
        type: Number
    },
    behandlungsdatum_6 : {
        type: Number
    },
    behandlungsdatum_7 : {
        type: Number
    },
    behandlungsdatum_8 : {
        type: Number
    },
    behandlungsdatum_9 : {
        type: Number
    },
    behandlungsdatum_10 : {
        type: Number
    },
    updated: {
        type: Date
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

mongoose.model('Patient', PatientSchema);
