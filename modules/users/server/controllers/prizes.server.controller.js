'use strict';

/**
 * Module dependencies
 */

var path = require('path'),
    email = require(path.resolve('./config/emails')),
    parsedJSON = require(path.resolve('./config/germany.json')),
    mongoose = require('mongoose'),
    async = require('async'),
  Patient = mongoose.model('Patient'),
    _ = require('lodash'),
  fs = require('fs'),
    lodash = require('lodash'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

exports.pincode = function(req, res) {
    res.json({city : lodash.find(parsedJSON, {zip: req.params.pincode+''})});
}

exports.updateStatus = function(req, res) {
    var patient = req.patient;
    patient.isPaid = !patient.isPaid;
    res.json(patient);
}

/**
 * Create an patient
 */
function setL1(data){
    if(data.rechnungsart == 'HP') data.L1 = "1; 20.5; 20.7; 20.8; 34.2; 35.2; 35.3; 35.6";
    else if(data.rechnungsart == 'Halbe GKV') data.L1 = "1; 35.2; 35.3";
    else if(data.rechnungsart == 'GKV') data.L1 = "1; 35.1; 35.2; 35.3";
    else if(data.rechnungsart == 'Ausfallgebühr') data.L1 = "Ausfallgebühr";
    else if(data.rechnungsart == 'HP- Physio') data.L1 = "A1; 4; 5; 20.1; 20.2; 20.3; 20.4; 20.6; 20.7 (2x); 20.8";
    else data.L1 = "Fehler!!!!!!!";
    return data;
}

function setB1(data) {
    if (data.rechnungsart == 'HP') data.B1 = 90.00;
    else if (data.rechnungsart == 'Halbe GKV') data.B1 = 45.00;
    else if (data.rechnungsart == 'GKV') data.B1 = 90.00;
    else if (data.rechnungsart == 'Ausfallgebühr') data.B1 = 45.00;
    else if (data.rechnungsart == 'HP- Physio') data.B1 = 90.00;
    else data.B1 = 0;
    return data;
}
function setL2(data){
    if(data.rechnungsart == 'HP') data.L2 = "20.1; 20.2; 20.5; 20.7; 20.8; 34.2; 35.2; 35.4; 35.6";
    else if(data.rechnungsart == 'Halbe GKV') data.L2 = "4; 35.1; 35.2";
    else if(data.rechnungsart == 'GKV') data.L2 = "4; 35.2; 35.3; 35.4";
    else if(data.rechnungsart == 'Ausfallgebühr') data.L2 = "Ausfallgebühr";
    else if(data.rechnungsart == 'HP- Physio') data.L2 = "3; 5; 17.1; A18.2 (2x); 20.1; 20.2; 20.3; 20.4; 20.7; 20.8";
    else data.L2 = "";
    return data;
}

function setB2(data){
    if(data.rechnungsart == 'HP') data.B2 = 93.50;
    else if(data.rechnungsart == 'Halbe GKV') data.B2 = 45.00;
    else if(data.rechnungsart == 'GKV') data.B2 = 90.00;
    else if(data.rechnungsart == 'Ausfallgebühr') data.B2 = 45.00;
    else if(data.rechnungsart == 'HP- Physio') data.B2 = 90.00;
    else data.B2 = 0;
    return data;
}

function setL3(data){
    if(data.rechnungsart == 'HP') data.L3 = "20.5; 20.7; 20.8; 27.3; 34.2; 35.2; 35.3; 35.4; 39.6";
    else if(data.rechnungsart == 'Halbe GKV') data.L3 = "35.2; 35.4";
    else if(data.rechnungsart == 'GKV') data.L3 = "35.1; 35.2; 35.3";
    else if(data.rechnungsart == 'Ausfallgebühr') data.L3 = "Ausfallgebühr";
    else if(data.rechnungsart == 'HP- Physio') data.L3 = "3; 4; A18.2(2x); 20.2; 20.3; 20.6; 20.8; 27.5; A33.2;39.6";
    else data.L3 = "";
    return data;
}

function setB3(data){
    if(data.rechnungsart == 'HP') data.B3 = 86.50;
    else if(data.rechnungsart == 'Halbe GKV') data.B3 = 45.00;
    else if(data.rechnungsart == 'GKV') data.B3 = 90.00;
    else if(data.rechnungsart == 'Ausfallgebühr') data.B3 = 45.00;
    else if(data.rechnungsart == 'HP- Physio') data.B3 = 90.50;
    else data.B3 = 0;
    return data;
}

function setLeistung1(data){
    if(data.behandlungsdatum_1)  data.leistung1 = data.L1;
    else    data.leistung1 = '';
    return data;
}

function setLeistung2(data){
    if(data.behandlungsdatum_2)  data.leistung2 = data.L2;
    else    data.leistung2 = '';
    return data;
}

function setLeistung3(data){
    if(data.behandlungsdatum_3)  data.leistung3 = data.L3;
    else    data.leistung3 = '';
    return data;
}

function setLeistung4(data){
    if(data.behandlungsdatum_4)  data.leistung4 = data.L1;
    else    data.leistung4 = '';
    return data;
}

function setLeistung5(data){
    if(data.behandlungsdatum_5)  data.leistung5 = data.L2;
    else    data.leistung5 = '';
    return data;
}

function setLeistung6(data){
    if(data.behandlungsdatum_6)  data.leistung6 = data.L3;
    else    data.leistung6 = '';
    return data;
}

function setLeistung7(data){
    if(data.behandlungsdatum_7)  data.leistung7 = data.L1;
    else    data.leistung7 = '';
    return data;
}

function setLeistung8(data){
    if(data.behandlungsdatum_8)  data.leistung8 = data.L2;
    else    data.leistung8 = '';
    return data;
}

function setLeistung9(data){
    if(data.behandlungsdatum_9)  data.leistung9 = data.L3;
    else    data.leistung9 = '';
    return data;
}

function setLeistung10(data){
    if(data.behandlungsdatum_10)  data.leistung10 = data.leistung1;
    else    data.leistung10 = '';
    return data;
}

function setBetrag1(data){
    if(data.behandlungsdatum_1)  data.betrag1 = data.B1;
    else data.betrag1 = 0;
    return data;
}

function setBetrag2(data){
    if(data.behandlungsdatum_2)  data.betrag2 = data.B2;
    else data.betrag2 = 0;
    return data;
}

function setBetrag3(data){
    if(data.behandlungsdatum_3)  data.betrag3 = data.B3;
    else data.betrag3 = 0;
    return data;
}

function setBetrag4(data){
    if(data.behandlungsdatum_4)  data.betrag4 = data.B1;
    else data.betrag4 = 0;
    return data;
}

function setBetrag5(data){
    if(data.behandlungsdatum_5)  data.betrag5 = data.B2;
    else data.betrag5 = 0;
    return data;
}

function setBetrag6(data){
    if(data.behandlungsdatum_6)  data.betrag6 = data.B3;
    else data.betrag6 = 0;
    return data;
}

function setBetrag7(data){
    if(data.behandlungsdatum_7)  data.betrag7 = data.B1;
    else data.betrag7 = 0;
    return data;
}

function setBetrag8(data){
    if(data.behandlungsdatum_8)  data.betrag8 = data.B2;
    else data.betrag8 = 0;
    return data;
}

function setBetrag9(data){
    if(data.behandlungsdatum_9)  data.betrag9 = data.B3;
    else data.betrag9 = 0;
    return data;
}

function setBetrag10(data){
    if(data.behandlungsdatum_10)  data.betrag10 = data.betrag1;
    else data.betrag10 = 0;
    return data;
}

function setSumme(data) {
    data.summe = (parseFloat(data.betrag10) || 0) + (parseFloat(data.betrag9) || 0) + ( parseFloat(data.betrag8) || 0) + ( parseFloat(data.betrag7) || 0) + ( parseFloat(data.betrag6) || 0) + ( parseFloat(data.betrag5) || 0) + ( parseFloat(data.betrag4) || 0) + ( parseFloat(data.betrag3) || 0) + ( parseFloat(data.betrag2) || 0) + ( parseFloat(data.betrag1) || 0);
    return data;
}

function setGesamt(data) {
    data.gesamt = data.summe;
    return data;
}

function setBA(data){
    var str = data.kindVornameNachname ? data.kindVornameNachname : "";
    data.ba = "an "+str+" geboren am";
    return data;
}

function setKind(data) {
    data.kind = data.ba;
    if(!data.gebDatumKind) data.kind = "";
    return data;
}

/*
function setBC(data){
    return data;
}

function setRechnr(data) {
    data.rechnr = "16/" ;
    return data;
}*/

function setGebuH(data){
    if(data.rechnungsart === "HP" || data.rechnungsart === "GKV" || data.rechnungsart === "Halbe GKV") {
        data.gebuh = "nach GebüH ";
    }else if(data.rechnungsart === "HP- Physio") {
        data.gebuh = "nach GebüH ";
    }else {
        data.gebuh = "";
    }
    return data;
}

function setGebuHZiffern(data){
    if(data.rechnungsart === "HP") {
        data.gebuHZiffern = "HP";
    }
       else if(data.rechnungsart === "HP- Physio") {
        data.gebuHZiffern =  "HP- Physio";
    }
    else if(data.rechnungsart === "GKV") {
        data.gebuHZiffern =  "GKV";
    }
    else if(data.rechnungsart === "HP- Physio") {
        data.gebuHZiffern =  "Halbe GKV";
    }
    return data;
}

function setAusfall(data){
    if(data.rechnungsart === "Ausfallgebühr") data.ausfall = 'Gemäß unseren AGBs werden nicht rechtzeitig abgesagte Termine zur Hälfte berechnet';
    else data.ausfall = 'Die Behandlung erfolgte aufgrund der';
    return data;
}

function setGrundMapping(data) {
    if(data.grund === "Diagnose") data.grundMapping = 'Die Behandlung erfolgte aufgrund der Diagnose:';
    if(data.grund === 'Empfehlung vom') data.grundMapping = 'Die Behandlung erfolgte aufgrund der Empfehlung vom';
    return data;
}

function beforeSave(req) {
    var tempData = req.body;

    if(tempData.rechnungsart === "Ausfallgebühr"){
        tempData.grund = null;
        tempData.diagnose = '';
        tempData.empfehlungsdatum = null;
        console.log(tempData);
    }

    tempData = setL1(tempData);
    tempData = setB1(req.body);
    tempData = setL2(req.body);
    tempData = setB2(req.body);
    tempData = setL3(req.body);
    tempData = setB3(req.body);
    tempData = setBetrag1(req.body);
    tempData = setBetrag2(req.body);
    tempData = setBetrag3(req.body);
    tempData = setBetrag4(req.body);
    tempData = setBetrag5(req.body);
    tempData = setBetrag6(req.body);
    tempData = setBetrag7(req.body);
    tempData = setBetrag8(req.body);
    tempData = setBetrag9(req.body);
    tempData = setBetrag10(req.body);
    tempData = setLeistung1(req.body);
    tempData = setLeistung2(req.body);
    tempData = setLeistung3(req.body);
    tempData = setLeistung4(req.body);
    tempData = setLeistung5(req.body);
    tempData = setLeistung6(req.body);
    tempData = setLeistung7(req.body);
    tempData = setLeistung8(req.body);
    tempData = setLeistung9(req.body);
    tempData = setLeistung10(req.body);
    tempData = setSumme(req.body);
    tempData = setGesamt(req.body);
    tempData = setBA(req.body);
    tempData = setKind(req.body);
    tempData = setGebuH(req.body);
    tempData = setGebuHZiffern(req.body);
    tempData = setAusfall(req.body);
    tempData = setGrundMapping(req.body);
    return tempData;
}

function accountNumberHandler(patient) {
    var date = new Date().getFullYear().toString().substr(2,2);
    Patient.findOne({'_id' : { $ne : patient._id}}).sort({'created' : -1}).exec(function (err, existUser) {
        Patient.findOne({_id: patient._id}).exec(function (error, loggedUser) {
            console.log(error);
            if (existUser) {
                var userId = existUser.orderNo + 1;
                loggedUser.orderNo = userId;
                var billNo = userId;
                if(billNo < 10)     billNo = '000'+userId;
                if(billNo >= 10 && billNo < 100)    billNo = '00'+userId;
                if(billNo >= 100 && billNo < 1000)   billNo = '0'+userId;
                loggedUser.billNo = date+'/'+ billNo;

                loggedUser.save(function(errnew1){
                    console.log(errnew1);
                });

                var newCopy = loggedUser;
                email.sendEmail(newCopy, 'menk.osteo@gmail.com');
               // email.sendEmail(newCopy, 'surendrakhatikk24@gmail.com');
                var newCopy2 = loggedUser;
               email.sendEmail(newCopy2, '90hi9h5h@mp.c-ij.com');
                return false;
            }
            else {
                loggedUser.orderNo = 1;
                loggedUser.billNo = date+'/0' + userId;
                loggedUser.save(function(errnew){
                    console.log(errnew);
                });
                email.sendEmail(loggedUser, 'menk.osteo@gmail.com');
                email.sendEmail(loggedUser, '90hi9h5h@mp.c-ij.com');
                return false;
            }
        });
    });
}


exports.sendEmailApi = function (req, res) {
    Patient.find({'_id' : { '$in' : req.body.patients}}).exec(function(err, patients){
        for(var k=0; k < patients.length; k++){
            email.sendEmail(patients[k], req.body.email);
        }
        res.json({'message' : 'Done'});
    });
}

exports.create = function (req, res) {
    var tempData = beforeSave(req);
    var patient = new Patient(tempData);
    patient.user = req.user;


    patient.save(function (err, savedPatient) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            accountNumberHandler(savedPatient);
            return res.status(200).send({
                message: 'Successfully Created'
            });
        }
    });
};



/**
 * Show the current patient
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var patient = req.patient ? req.patient.toJSON() : {};

  // Add a custom field to the Patient, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Patient model.
  patient.isCurrentUserOwner = !!(req.user && patient.user && patient.user._id.toString() === req.user._id.toString());

  res.json(patient);
};

/**
 * Update an patient
 */
exports.update = function (req, res) {
	var patient = req.patient;
    var reqData = beforeSave(req);
    //reqData.user = req.user._id;
    patient = _.extend(patient, reqData);
    patient.updated = Date.now();

  patient.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(patient);
    }
  });
};

/**
 * Delete an patient
 */

exports.delete = function (req, res) {
  var patient = req.patient;

  patient.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(patient);
    }
  });
};

/**
 * List of Patients
 */
exports.list = function (req, res) {
    var filter = {};
    if(req.query.q){
        filter['$or'] = [
            {
                'nachname' : {'$regex' : req.query.q, '$options' : 'i'}
            },
            {
                'billNo' : {'$regex' : req.query.q, '$options' : 'i'}
            }
        ];

    if(parseInt(req.query.q)) {
            filter['$or'].push({
                'orderNo'
            :
                parseInt(req.query.q)
            });
        }
    }
    async.parallel({
        result: function(callback) {
            var page = 0;
            if(req.query.page) page = parseInt(req.query.page) - 1
            Patient.find(filter)
                .sort('-orderNo')
                .populate('user', 'displayName')
                .limit(10)
                .skip(10 * page)
                .exec(function (err, patients) {
                    if (err) {
                        callback(err, null)
                    } else {
                        callback(null, patients)
                    }
                });
        },
        count: function(callback) {
            Patient.count(filter).exec(function (err, count) {
                if (err) {
                    callback(err, null)
                } else {
                    callback(null, count)
                }
            });
        },
        total: function(callback) {
            Patient.aggregate( [
                { $match: { isPaid : false } },
                { $group: { '_id' : '_id',
                    total: { $sum: "$summe" } } }
            ]).exec(callback)
        }
    }, function(err, results) {
        res.json(results);
    });
};


/**
 * Patient middleware
 */
exports.patientByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Patient is invalid'
    });
  }

  Patient.findById(id).populate('user', 'displayName').exec(function (err, patient) {
      if (err) {
      return next(err);
    } else if (!patient) {
      return res.status(404).send({
        message: 'No patient with that identifier has been found'
      });
    }
    req.patient = patient;
    next();
  });
};
