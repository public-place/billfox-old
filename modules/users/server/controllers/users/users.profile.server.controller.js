'use strict';

/**
 * Module dependencies
 */
var _ = require('lodash'),
  fs = require('fs'),
  path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  mongoose = require('mongoose'),
  multer = require('multer'),
  config = require(path.resolve('./config/config')),
  User = mongoose.model('User');
//    Goal = mongoose.model('Goal');

/**
 * Update user details
 */
exports.update = function (req, res) {
  // Init Variables
  var user = req.user;

  // For security measurement we remove the roles from the req.body object
  delete req.body.roles;

  // For security measurement do not use _id from the req.body object
  delete req.body._id;

  if (user) {
    // Merge existing user
    user = _.extend(user, req.body);
    user.updated = Date.now();
    user.displayName = user.firstName + ' ' + user.lastName;

    user.save(function (err) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        req.login(user, function (err) {
          if (err) {
            res.status(400).send(err);
          } else {
            res.json(user);
          }
        });
      }
    });
  } else {
    res.status(400).send({
      message: 'User is not signed in'
    });
  }
};





/**
 * Update profile picture
 */
exports.changeProfilePicture = function (req, res) {
  var user = req.user;
  var upload = multer(config.uploads.profileUpload).single('newProfilePicture');
  var profileUploadFileFilter = require(path.resolve('./config/lib/multer')).profileUploadFileFilter;
    console.log(req.query.imageType);
  // Filtering to upload only images
  upload.fileFilter = profileUploadFileFilter;

  if (user) {
    upload(req, res, function (uploadError) {
      if (uploadError) {
        return res.status(400).send({
          message: 'Error occurred while uploading profile picture'
        });
      } else {
          console.log(config.uploads.profileUpload.dest + req.file.filename);
          if(req.query.imageType == 'image') {
              user.profileImageURL = config.uploads.profileUpload.dest + req.file.filename;
          }

          if(req.query.imageType == 'goal') {
              //updateUserGoal(req, config.uploads.profileUpload.dest + req.file.filename);
              console.log(config.uploads.profileUpload.dest + req.file.filename);
              return res.json({'imageUrl' : config.uploads.profileUpload.dest + req.file.filename});
          }

        user.save(function (saveError) {
          if (saveError) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(saveError)
            });
          } else {
            req.login(user, function (err) {
              if (err) {
                res.status(400).send(err);
              } else {
                res.json(user);
              }
            });
          }
        });
      }
    });
  } else {
    res.status(400).send({
      message: 'User is not signed in'
    });
  }
};

function updateUserGoal(req, imageUrl){
    Goal.findOne({user : req.user._id}).exec(function(err, goal){
        goal.imageUrl = imageUrl;
        goal.save();
    });
}

/**
 * Send User
 */
exports.me = function (req, res) {
  res.json(req.user || null);
};
