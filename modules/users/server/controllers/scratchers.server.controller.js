'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
    async = require('async'),
    Scratcher = mongoose.model('Scratcher'),
  fs = require('fs'),
    request = require('request'),
    _ = require('lodash'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an scratcher
 */
exports.create = function (req, res) {
    var scratcher = new Scratcher(req.body);

    scratcher.save(function (err, savedScratcher) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {


            var options = {
                method: 'POST',
                uri: 'https://rest.datamatch.nl/scratchcard/win/'+savedScratcher.prizeId,
				
				//uri: 'https://rest.datamatch.nl/hello',
                form: {
                    userName: 'somevalue',
                    userEmail : savedScratcher.email,
                    userIdentifier : savedScratcher.id
					//name :'Surendra',
					//age : 23
                },
                headers: {
                   // 'Authorization':  'Pmi/7ZD/phlFkbGoKn07yTP+2g9CkFLBnqFRMjk2bfb0eBtW6QikrIRk/ndpWym20YehiilcqCAdj1Pr/a/5DX3JTngIUUlweS+TDYxri0mGzrmpm1MKmNoVhzOXaZKFefzesLx43lrqWsOhVgtHXSlBnvC+yi8V+g3qCP10U0rmI7XjZyxk7K4+h77oX3751BNUNrS5N47/KlrL9VeA8T0dqaYIxSskq4RdPtAChvtaA1OmjOGjo7RwSxUsasKgQmmMmG2laQ2n3xGFFeqXlnKTMbAyJrygXDL2KTHiF/iEHlypgkJ3ypGuFrgnzlR6HcJF16LwhyNkFcvsU+DAac8ji+PuWBa0LYNEyuAAG5D/shE8K5zxpzFannFcRbUj2UEd7Iy6IAsjAP7xVc22OU821bP3Zt3KB0qrmarVeGvRh6GKKVyoIB2PU+v9r/kNGGkazCEXiP7YXRK8e9PZ+1ozAeY0tEWiqZ73UIsLjR+/4eBUC8PHm+7SxVjEn2LPI9iAz9MMwO8olX+W5UxAVL2LDOIAbscjfUUYNvX8R18='
                    'Authorization' : 'Pmi/7ZD/phlFkbGoKn07yTP+2g9CkFLBnqFRMjk2bfb0eBtW6QikrIRk/ndpWym20YehiilcqCAdj1Pr/a/5DX3JTngIUUlweS+TDYxri0n5cTgJvly+vEczIUu8YkZQefzesLx43lrqWsOhVgtHXSlBnvC+yi8V+g3qCP10U0rmI7XjZyxk7K4+h77oX375QncuXlQQnRcWAG2/QL5JhbdNByvyKoxYnKPz1sAapHIQ+0sXKg8e85TIgeWktX5MEPxNccS+Vw0PjcmX+ooT9b060mrNlZEvEqnbEHhiqKgFWBil2Wfu+6ABsVSZSpbNqE0Lk0Vb5tqIzZ8WwpN4bW4M597uxSSEdkM38OVfCIlFJMAoZ+L6KfUProOIIstCvT1yEYVBa3F1nIi2ndTqM4CDdkCcuK8s86GnuT7QPztK/9aa9NZJYLvPhhUO2PuGkUcvt9iprFBmdA4GgOJ4qKdbtzOyGJ3pK2+4arkGUF/pFrFJx66fwI+q6mZVUTb2Pz8sut9+tccZQfkKuonLLOYYGT3x4LmEDjr+FJYtPlWqB7J9Lig6iJw63OytGUerLylkblS4uQE4WmZ4sdySDd6NlS7GsEVXxCGWnuJyTrNpHcp1cSNk5m51aXCpLxd/xTLdr6cdOhe+CsQTTRknz0MU5+P/kDJAjg2WEZOUHlSJVMau+gIxlC9JzbFfsQKHrBE1iY6RHDlS1Y9FvRs5UPaSCn9PMbCx8leZJmnqhqHtE6elT3SrkTbEkIYJx5VmGiNYPHmFEFNlxFJeXNI6nMEmwYnfGhr9D6xFO+hCG7ysL5NvdClCcBuesD8IzuXATFofiZRiOq8sbYL020D/sO79Ta0rq1LNzMyyflQcGU/D6e9WFDaYopWnN51rnZTnvIhA9JID14eWbEXAJNBWAINsrc4piQhEyw8Jf+MI3hNma+VESqdVQvrzgf68svyr'
                }
            };
            request(options, function(error, response, body) {
                // do stuff
                console.log("Error", error);
                console.log(body);
            });


            return res.status(200).send({
                message: 'Request Sent'
            });
        }
    });
};

/**
 * Show the current scratcher
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var scratcher = req.scratcher ? req.scratcher.toJSON() : {};

  // Add a custom field to the Scratcher, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Scratcher model.
  scratcher.isCurrentUserOwner = !!(req.user && scratcher.user && scratcher.user._id.toString() === req.user._id.toString());

  res.json(scratcher);
};

/**
 * Update an scratcher
 */
exports.update = function (req, res) {
	var scratcher = req.scratcher;
    scratcher = _.extend(scratcher, req.body);
    scratcher.updated = Date.now();

  scratcher.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(scratcher);
    }
  });
};


function updatePercentage(req ,res, scratcher, per) {
    Scratcher.find({_id :{ $nin : [scratcher._d]}}).sort('-created').populate('user', 'displayName').exec(function (err, scratchers) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            var total = per;
            //var idPercentage = {};
            for(var k=0; k < scratchers.length; k++){
                total = parseFloat(total) + parseFloat(scratchers[k].winPercentage);
                //idPercentage[scratchers[k]._id.toString()] = parseFloat(scratchers[k].winPercentage);
            }

            var average;
            if(total > 100){
                average = (parseFloat(per)/scratchers.length);
            }else {
                return res.json(scratcher);
            }

            for(var m=0; m < scratchers.length; m++){

                scratchers[m].winPercentage = (parseFloat(scratchers[m].winPercentage) - average).toFixed(3);
                scratchers[m].save(function(err){
                    console.log(err);
                });
            }
            return res.json(scratcher);

        }
    });
}
/**
 * Delete an scratcher
 */

exports.delete = function (req, res) {
  var scratcher = req.scratcher;

  scratcher.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(scratcher);
    }
  });
};

/**
 * List of Scratchers
 */
exports.list = function (req, res) {
    async.parallel({
        result: function(callback) {
            Scratcher.find({},{}).sort('-created').populate('prizeId', 'name').exec(function (err, scratchers) {
                    if (err) {
                        callback(err, null)
                    } else {
                        callback(null, scratchers)
                    }
                });
        },
        scratcherCount: function(callback) {
            Scratcher.find().exec(function (err, scratchers) {
                if (err) {
                    callback(err, null)
                } else {
                    callback(null, mapWithCategory(scratchers))
                }
            });
        }
    }, function(err, results) {
        res.json(results);
    });
};

exports.doelen = function(req,res){
    var filter = { '$or' : [{isScratcherTarget : 'Yes'}, {isScratcherTarget : 'yes'}]};
    /*if(req.query.isReport && !req.query.transactionType){
        filter.transactionType = { $in: [ 'Commissions', 'Recurring Earnings', 'Maintenance Fee']};
    }
    else if(req.query.isIncomeStatement && !req.query.transactionType){
        filter.transactionType = { $in: [ 'Commissions', 'Recurring Earnings']};
    }
    else {
        filter.transactionType = { $in : ['Manual Adjustment','Transfer','Withdrawal','Interest','Deposit'] };
    }
    filter = reqQueryHandler(req.query, filter);
    */

    async.parallel({
        result: function(callback) {
            Scratcher.find(filter,
                {
                    title : 1,
                    amount : 1,
                    url : 1,
                    imageURL : 1,
                    scratcherCategory : 1
                }).sort('title').exec(function (err, scratchers) {
                if (err) {
                    callback(err, null)
                } else {
                    callback(null, scratchers)
                }
            });
        },
        scratcherCount: function(callback) {
            Scratcher.find(filter).sort('-created').populate('user', 'displayName').exec(function (err, scratchers) {
                if (err) {
                    callback(err, null)
                } else {
                    callback(null, mapWithCategory(scratchers))
                }
            });
        }
    }, function(err, results) {
            res.json(results);
        // results is now equals to: {one: 'abc\n', two: 'xyz\n'}
    });
}


exports.getIsAlready = function(req, res) {
    if(req.query.token) {
        Scratcher.findOne({
            id: req.query.token
        }).exec(function (err, scratcher) {
            if (err || !scratcher) {
                res.json({ text : false});
            } else {
                res.json({ text : true});
            }
        });
    }else {
        res.json({ scratcherInfo: 'Not Found'});
    }
}

exports.getScratcherCard = function(req, res){
    if(req.params.id) {
        Scratcher.findOne({
            id: req.params.id
        }, {
            email: 1,
            id: 1,
            _id: 0,
            prizeId: 1
        }).populate('prizeId', 'name')
            .exec(function (err, user) {
            if (err || !user) {
                res.json({ scratcherInfo: 'Not Found'});
            } else {
                res.json({prizename : user.prizeId.name, email : user.email, id : user.id});
            }
        });
    }else {
        res.json({ scratcherInfo: 'Not Found'});
    }
}


exports.getPublicInfo = function(req, res) {
    if(req.params.url) {
        Scratcher.findOne({
            username: req.params.username
        }, {
            title: 1,
            description: 1,
            amount: 1,
            imageURL: 1
        }).exec(function (err, user) {
            if (err || !user) {
                res.json({ scratcherInfo: 'Not Found'});
            } else {
                res.json({ scratcherInfo: user});
            }
        });
    }else {
        res.json({ scratcherInfo: 'Not Found'});
    }
}

exports.getRandomNumber = function(req, res) {
    Scratcher.aggregate(
        { $sample: { size: 1 } }
    ).exec(function (err, scratcher) {
            if (err || !scratcher || !scratcher.length) {
                res.json({ scratcherInfo: 'Not Found'});
            } else {
                res.json({ scratcherInfo: scratcher[0].name});
            }
        });
}

function mapWithCategory(scratchers){
    var result = {};
    result['total'] = scratchers.length || 0;
    result['prizeCollected'] = 0;
    result['prizeNotCollected'] = 0;

    if(scratchers && scratchers.length) {
        for (var k = 0; k < scratchers.length; k++) {
            if (scratchers[k].isPrizeCollected) {
                result['prizeCollected'] = result['prizeCollected'] + 1;
            }

            if (!scratchers[k].isPrizeCollected) {
                result['prizeNotCollected'] = result['prizeNotCollected'] + 1;
            }
        }
    }
    return result;
}


/**
 * Scratcher middleware
 */
exports.scratcherByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Scratcher is invalid'
    });
  }

  Scratcher.findById(id).populate('user', 'displayName').exec(function (err, scratcher) {
    if (err) {
      return next(err);
    } else if (!scratcher) {
      return res.status(404).send({
        message: 'No scratcher with that identifier has been found'
      });
    }
    req.scratcher = scratcher;
    next();
  });
};
