(function(window) {
    'use strict';

    var applicationModuleName = 'mean';

    var service = {
        applicationModuleName: applicationModuleName,
        applicationModuleVendorDependencies: ['ngResource', 'ngAnimate','ngSanitize','ngMessages', 'ui.router', 'angularFileUpload', 'slick', 'ui.select','ngMaterial', 'md-steppers', 'mdDataTable','datetime','datepicker','cl.paging'],
        registerModule: registerModule
    };

    window.ApplicationConfiguration = service;
	
	toastr.options = {
	
		"positionClass": "toast-top-center",  //"positionClass": "toast-top-full-width",
	}

    // Add a new vertical module
    function registerModule(moduleName, dependencies) {
        // Create angular module
        angular.module(moduleName, dependencies || []);

        // Add the module to the AngularJS configuration file
        angular.module(applicationModuleName).requires.push(moduleName);
    }
}(window));
