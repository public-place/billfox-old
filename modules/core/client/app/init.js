(function (app) {
  'use strict';

  // Start by defining the main module and adding the module dependencies
  angular
    .module(app.applicationModuleName, app.applicationModuleVendorDependencies);

  // Setting HTML5 Location Mode
  angular
    .module(app.applicationModuleName)
    .config(bootstrapConfig)
  ;

  function bootstrapConfig($locationProvider, $httpProvider, $mdDateLocaleProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');

   $mdDateLocaleProvider.formatDate = function(date) {
        if(date) {
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            if(day < 10) day = '0'+day;
            var month = (monthIndex + 1);
            if((monthIndex + 1) < 10) month = '0'+(monthIndex + 1);
            return day + '.' + month + '.' + year;
        }
        return 'Enter Date';

  };

    $httpProvider.interceptors.push('authInterceptor');


  }

  bootstrapConfig.$inject = ['$locationProvider', '$httpProvider', '$mdDateLocaleProvider'];

  // Then define the init function for starting up the application
  angular.element(document).ready(init);

  function init() {
    // Fixing facebook bug with redirect
    if (window.location.hash && window.location.hash === '#_=_') {
      if (window.history && history.pushState) {
        window.history.pushState('', document.title, window.location.pathname);
      } else {
        // Prevent scrolling by storing the page's current scroll offset
        var scroll = {
          top: document.body.scrollTop,
          left: document.body.scrollLeft
        };
        window.location.hash = '';
        // Restore the scroll offset, should be flicker free
        document.body.scrollTop = scroll.top;
        document.body.scrollLeft = scroll.left;
      }
    }

    // Then init the app
    angular.bootstrap(document, [app.applicationModuleName]);
  }
}(ApplicationConfiguration));
