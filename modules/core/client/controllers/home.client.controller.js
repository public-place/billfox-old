(function() {
    'use strict';

    angular
        .module('core')
        .controller('HomeController', HomeController);

    function HomeController($scope, $state, $http, PatientsService, $location, $window, $stateParams, Authentication) {
        var vm = this;
        $scope.authentication = Authentication;
        if (!$scope.authentication.user) {
            $location.path('/admin/login');
        }

        vm.items = ['HWS-Syndrom', 'BWS-Syndrom', 'LWS-Syndrom', 'HWS-Dysfunktion', 'BWS-Dysfunktion', 'LWS-Dysfunktion', 'Rippen-Dysfunktion', 'Becken-Dysfunktion', 'Verdauungsinsuffizienz', 'Vorzugshaltung', 'Schädelasymmetrie', 'Migräne', 'craniomandibuläre Dysfunktion', 'Andere :'];
        vm.selected = [];
        vm.toggle = function(item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            } else {
                list.push(item);
            }
        };

        vm.diagnoseOther = function() {
            var index = vm.selected.indexOf('Andere :');
            if (vm.patient.diagnoseOther) {
                if (index === -1) {
                    vm.selected.push('Andere :');
                }
            } else {
                vm.selected.splice(index, 1);
            }
        }

        vm.exists = function(item, list) {
            return list.indexOf(item) > -1;
        };

        vm.isIndeterminate = function() {
            return (vm.selected.length !== 0 &&
                vm.selected.length !== vm.items.length);
        };

        vm.isChecked = function() {
            return vm.selected.length === vm.items.length;
        };

        vm.toggleAll = function() {
            if (vm.selected.length === vm.items.length) {
                vm.selected = [];
            } else if (vm.selected.length === 0 || vm.selected.length > 0) {
                vm.selected = vm.items.slice(0);
            }
        };

        vm.randomPrize = {};
        vm.reload = reload;
        vm.init = init;

        function init() {
            vm.patient = {};
            vm.patient = angular.copy(initialFetch(vm.patient));
            vm.selectedStep = 0;
            vm.stepProgress = 1;
            vm.maxStep = 6;
            vm.showBusyText = false;
            vm.stepData = [{
                    step: 1,
                    completed: false,
                    optional: false,
                    data: {}
                },
                {
                    step: 2,
                    completed: false,
                    optional: false,
                    data: {}
                },
                {
                    step: 3,
                    completed: false,
                    optional: false,
                    data: {}
                },
                {
                    step: 4,
                    completed: false,
                    optional: false,
                    data: {}
                },
                {
                    step: 5,
                    completed: false,
                    optional: false,
                    data: {}
                },
                {
                    step: 6,
                    completed: false,
                    optional: false,
                    data: {}
                }
            ];

            if ($stateParams.patientId) {
                vm.patient = {};
                findOne($stateParams.patientId);
            }


        }

        function findOne(id) {
            PatientsService.get({
                patientId: id
            }).$promise.then(function(data) {
                    console.log("====>>>", data)
                data = afterFetch(data);
                vm.patient = data
            });
        };

        function afterFetch(data) {
            var tempCopy = angular.copy(data);
            tempCopy.heute = moment(new Date(tempCopy.heute)).format('DD.MM.YYYY');
            for (var k = 1; k <= 10; k++) {
                if (tempCopy['behandlungsdatum_' + k]) {
                    tempCopy['behandlungsdatum_' + k] = moment(new Date(tempCopy['behandlungsdatum_' + k])).format('DD.MM.YYYY');
                }
            }
            if (tempCopy.gebDatumKind) tempCopy.gebDatumKind = moment(new Date(tempCopy.gebDatumKind)).format('DD.MM.YYYY');
            if (tempCopy.empfehlungsdatum) tempCopy.empfehlungsdatum = moment(new Date(tempCopy.empfehlungsdatum)).format('DD.MM.YYYY');

            if(tempCopy && tempCopy.diagnose){
				vm.selected = tempCopy.diagnose.split(',');
                for(var k=0; k < vm.selected.length; k++){
                    vm.selected[k] =    vm.selected[k].trim();
                }
			}
            return tempCopy;
        }

        function initialFetch(data) {
            var tempCopy = angular.copy(data);
            tempCopy.heute = moment(new Date()).format('DD.MM.YYYY');
            tempCopy.behandlungsdatum_1 = moment(new Date()).format('DD.MM.YYYY');
            return tempCopy;
        }

        vm.reset = function(n, value) {
            console.log(value);
            if (!value) {
                for (var k = n; k <= 10; k++) {
                    if (vm.patient['behandlungsdatum_' + k]) {
                        vm.patient['behandlungsdatum_' + k] = null;
                    }
                }
            }
        }

        init();

        vm.enableNextStep = function nextStep() {
            //do not exceed into max step

            if (vm.selectedStep >= vm.maxStep) {
                return;
            }
            //do not increment vm.stepProgress when submitting from previously completed step
            if (vm.selectedStep === vm.stepProgress - 1) {
                vm.stepProgress = vm.stepProgress + 1;
            }
            vm.selectedStep = vm.selectedStep + 1;
        }

        vm.moveToPreviousStep = function moveToPreviousStep() {
            if (vm.patient.grund == 'Empfehlung vom' && vm.selectedStep == 4) {
                vm.selectedStep = 3;
                vm.stepProgress = 4;
            }

            if (vm.patient.rechnungsart == 'Ausfallgebühr' && vm.selectedStep == 4) {
                vm.selectedStep = 0;
                vm.stepProgress = 1;
                return false;
            }

            if (vm.patient.grund == 'Diagnose' && vm.selectedStep == 3) {
                vm.selectedStep = 1;
                vm.stepProgress = 2;
                return false;
            }

            if (vm.selectedStep > 0) {
                vm.selectedStep = vm.selectedStep - 1;
            }
        }

        vm.submitCurrentStep = function submitCurrentStep(step, isSkip) {
            if (!vm.patient._id) {
                vm.showBusyText = true;
            }
            if (step == 'step1') {
                vm.stepData[0].data.completed = true;
                vm.showBusyText = false;

                if (vm.patient.rechnungsart == 'Ausfallgebühr') {
                    vm.selectedStep = 4;
                    vm.stepProgress = 5;
                } else {
                    vm.enableNextStep();
                }

                return false;
            }

            if (step == 'step2') {
                vm.stepData[1].data.completed = true;
                vm.showBusyText = false;

                if (vm.patient.grund == 'Empfehlung vom') {
                    vm.selectedStep = 2;
                    vm.stepProgress = 3;
                } else {
                    vm.selectedStep = 3;
                    vm.stepProgress = 4;
                }

                return false;
            }

            if (step == 'step3') {
                vm.stepData[2].data.completed = true;
                vm.showBusyText = false;
                if (vm.patient.grund == 'Empfehlung vom') {
                    vm.selectedStep = 4;
                    vm.stepProgress = 5;
                }
                //vm.enableNextStep();
                return false;
            }

            if (step == 'step4') {
                vm.stepData[3].data.completed = true;
                vm.showBusyText = false;
                vm.enableNextStep();
                return false;
            }

            if (step == 'step5') {
                vm.showBusyText = false;
                $scope.isLoading = true;
                vm.stepData[3].data.completed = true;
                var resultData = formatBeforeSave();

                if (resultData._id) {
                    //var patient = new PatientsService (resultData);
                    resultData.$update(function(response) {
                        toastr.success('Document successfully update');
                        $scope.isLoading = false;
                        vm.enableNextStep();
                        vm.stepData[4].data.completed = true;
                    }, function(errorResponse) {
                        toastr.error('Error : ' + errorResponse.data.message);
                    });
                } else {
                    var patient = new PatientsService(resultData);
                    patient.$save(function(response) {
                        toastr.success('Document  successfully added');
                        vm.enableNextStep();
                        vm.stepData[4].data.completed = true;
                    }, function(errorResponse) {
                        toastr.error('Error : ' + errorResponse.data.message);
                    });
                }
            }
        }

        function formatBeforeSave() {
            var tempCopy = angular.copy(vm.patient);
            tempCopy.heute = datefor(tempCopy.heute);

            if (tempCopy.gebDatumKind) {
                tempCopy.gebDatumKind = datefor(tempCopy.gebDatumKind);
            }

            if (tempCopy.empfehlungsdatum) {
                tempCopy.empfehlungsdatum = datefor(tempCopy.empfehlungsdatum);
            }

            for (var k = 1; k <= 10; k++) {
                if (tempCopy['behandlungsdatum_' + k]) {
                    tempCopy['behandlungsdatum_' + k] = datefor(tempCopy['behandlungsdatum_' + k]);
                }
            }

            //diagnose
            var str = '';
            if (vm.selected.length && vm.selected.length > 1) {
                for (var k = 0; k < vm.selected.length; k++) {
                    /*if(vm.selected[k].indexOf('Andere :') > -1){
                        str += 'Andere : '+vm.patient.diagnoseOther || '' +',';
                    }else {

                    }*/
                    str += vm.selected[k] + ', ';
                }
                str = str.trim();
                console.log(tempCopy.diagnose);
                tempCopy.diagnose = str.slice(0, -1);
                console.log(tempCopy.diagnose);
            } else {
                tempCopy.diagnose = vm.selected[0];
            }


            return tempCopy;
        }

        function datefor(d) {
            return new Date(moment(d, 'DD.MM.YYYY').format('MM-DD-YYYY')).getTime();
            //return new Date(d).getTime();
        }

        var query = $location.search();

        function reload() {
            $location.$$search = {};
            $location.path('/');
            $window.location.reload();
        }

        vm.getCity = function(pincode) {
            $http.get('/api/getCity/' + pincode).success(function(data) {
                vm.patient.stadt = data.city ? data.city.city : '';

            }).error(function() {

            });
        }

    }
}());
