(function () {
  'use strict';

  angular
    .module('core')
    .controller('HeaderController', HeaderController);

  HeaderController.$inject = ['$scope', '$state', 'Authentication','$location', '$window'];

  function HeaderController($scope, $state, Authentication,$location, $window) {
    var vm = this;
    vm.authentication = Authentication;
    vm.isHome = isHome;
      vm.logoutMe = function(){
          $location.path('/api/auth/signout');
          $window.location.reload();
      }


    $scope.$on('$stateChangeSuccess', stateChangeSuccess);

      function isHome(){
          if($location.path() === '/'){
              return true;
          }else {
              return false;
          }
      }

    function stateChangeSuccess() {

      // Collapsing the menu after navigation
      vm.isCollapsed = false;
    }
  }
}());
